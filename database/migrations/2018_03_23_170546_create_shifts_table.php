<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_pattern', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description')->nullable();
            $table->string('name');
            $table->time('start_time');
            $table->time('end_time');
            $table->float('factor_salary');
            $table->timestamps();
        });

         Schema::create('shift_master', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->date('start_date');
            $table->date('end_date');

            $table->integer('shift_pattern_id')->unsigned();
            $table->foreign('shift_pattern_id')->references('id')->on('shift_pattern');
            $table->timestamps();
        });

        Schema::create('shift', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            
            $table->integer('shift_pattern_id')->unsigned();
            $table->foreign('shift_pattern_id')->references('id')->on('shift_pattern')->onDelete('cascade');
            $table->integer('shift_master_id')->unsigned();
            $table->foreign('shift_master_id')->references('id')->on('shift_master')->onDelete('cascade');
        });

        Schema::create('employee_shift', function (Blueprint $table) {
            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')->references('id')->on('users');

            $table->integer('shift_id')->unsigned();
            $table->foreign('shift_id')->references('id')->on('shift');
        });

        Schema::create('worked_time', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('in_time');
            $table->datetime('out_time')->nullable();

            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')->references('id')->on('users');

            $table->integer('shift_id')->unsigned();
            $table->foreign('shift_id')->references('id')->on('shift');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_request_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('pending_shift_request', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('shift_change_id')->unsigned()->nullable();
            $table->foreign('shift_change_id')->references('id')->on('shift');

            $table->text('description')->nullable();
            $table->timestamps();

            $table->integer('old_shift_id')->unsigned();
            $table->foreign('old_shift_id')->references('id')->on('shift');

            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')->references('id')->on('users');

            $table->integer('request_type_id')->unsigned();
            $table->foreign('request_type_id')->references('id')->on('shift_request_type');
        });

        Schema::create('finished_shift_request', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('shift_change_id')->unsigned()->nullable();
            $table->foreign('shift_change_id')->references('id')->on('shift');
            
            $table->text('description')->nullable();
            $table->boolean('status');
            $table->timestamps();

            $table->integer('old_shift_id')->unsigned();
            $table->foreign('old_shift_id')->references('id')->on('shift');

            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')->references('id')->on('users');

            $table->integer('request_type_id')->unsigned();
            $table->foreign('request_type_id')->references('id')->on('shift_request_type');
        });

        Schema::create('exception_shift', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shift_change_id')->unsigned()->nullable();
            $table->foreign('shift_change_id')->references('id')->on('shift');
            
            $table->timestamps();

            $table->integer('shift_id')->unsigned();
            $table->foreign('shift_id')->references('id')->on('shift');

            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')->references('id')->on('users');

            $table->integer('request_id')->unsigned();
            $table->foreign('request_id')->references('id')->on('finished_shift_request');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}

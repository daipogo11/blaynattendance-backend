<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worked_time');
        Schema::dropIfExists('feedbacks');
        Schema::dropIfExists('comments');
        Schema::dropIfExists('notifications');
        Schema::dropIfExists('payrolls');
        Schema::dropIfExists('pending_leave_request');
        Schema::dropIfExists('finished_leave_request');
        Schema::dropIfExists('pending_payroll_request');
        Schema::dropIfExists('finished_payroll_request');
        Schema::dropIfExists('store');
        Schema::dropIfExists('exception_shift'); 
        Schema::dropIfExists('pending_shift_request');
        Schema::dropIfExists('finished_shift_request');
        Schema::dropIfExists('shift_request_type');
        Schema::dropIfExists('employee_shift');
        Schema::dropIfExists('shift');
        Schema::dropIfExists('shift_master');
        Schema::dropIfExists('shift_pattern');        
        Schema::dropIfExists('user_has_role');
        Schema::dropIfExists('roles');        
        Schema::dropIfExists('users');        
        Schema::dropIfExists('wage');       
        Schema::dropIfExists('user_groups');
        Schema::dropIfExists('wage_groups');
    }
}

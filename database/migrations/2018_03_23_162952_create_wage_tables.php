<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWageTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wage_groups', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('wage', function(Blueprint $table) {
            $table->increments('id');
            $table->double('salary');
            $table->date('start_date');
            $table->timestamps();

            $table->integer('wage_group_id')->unsigned();
            $table->foreign('wage_group_id')->references('id')->on('wage_groups')->onDelete('cascade');
        });

        Schema::create('user_groups', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

            $table->integer('wage_group_id')->unsigned()->nullable();
            $table->foreign('wage_group_id')->references('id')->on('wage_groups')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

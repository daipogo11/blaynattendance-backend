<?php

use Illuminate\Database\Seeder;

class WorkedTimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('worked_time')->insert([
            [
                'id'=>1,
                'in_time'=>'2018-04-30 7:00:00',
                'out_time'=>'2018-04-30 12:00:00',
                'employee_id' => 3,
                'shift_id'=>1,   
            ],
            [
                'id'=>2,
                'in_time'=>'2018-05-01 7:00:00',
                'out_time'=>'2018-05-01 12:00:00',
                'employee_id' => 3,
                'shift_id'=>2,   
            ],
            [
                'id'=>3,
                'in_time'=>'2018-05-02 7:00:00',
                'out_time'=>'2018-05-02 12:00:00',
                'employee_id' => 3,
                'shift_id'=>3,   
            ],
            [
                'id'=>4,
                'in_time'=>'2018-05-03 7:00:00',
                'out_time'=>'2018-05-03 12:00:00',
                'employee_id' => 3,
                'shift_id'=>4,   
            ],
            [
                'id'=>5,
                'in_time'=>'2018-05-04 7:00:00',
                'out_time'=>'2018-05-04 12:00:00',
                'employee_id' => 3,
                'shift_id'=>5,   
            ],
            [
                'id'=>6,
                'in_time'=>'2018-05-05 7:00:00',
                'out_time'=>'2018-05-05 12:00:00',
                'employee_id' => 3,
                'shift_id'=>6,   
            ],
            [
                'id'=>7,
                'in_time'=>'2018-05-06 7:00:00',
                'out_time'=>'2018-05-06 12:00:00',
                'employee_id' => 3,
                'shift_id'=>7,   
            ],
            [
                'id'=>8,
                'in_time'=>'2018-05-07 7:00:00',
                'out_time'=>'2018-05-07 12:00:00',
                'employee_id' => 3,
                'shift_id'=>8,   
            ],
            [
                'id'=>9,
                'in_time'=>'2018-05-08 7:00:00',
                'out_time'=>'2018-05-08 12:00:00',
                'employee_id' => 3,
                'shift_id'=>9,   
            ],
            [
                'id'=>10,
                'in_time'=>'2018-05-09 7:00:00',
                'out_time'=>'2018-05-09 12:00:00',
                'employee_id' =>3 ,
                'shift_id'=>10,   
            ]
        ]);
        DB::table('payrolls')->insert([
            [
                'id'=>1,
                'date'=>'2018-04-30',
                'amount'=>150000,
                'user_id'=>3
            ],
            [
                'id'=>2,
                'date'=>'2018-05-01',
                'amount'=>150000,
                'user_id'=>3
            ],
            [
                'id'=>3,
                'date'=>'2018-05-02',
                'amount'=>150000,
                'user_id'=>3
            ],
            [
                'id'=>4,
                'date'=>'2018-05-03',
                'amount'=>150000,
                'user_id'=>3
            ],
            [
                'id'=>5,
                'date'=>'2018-05-04',
                'amount'=>150000,
                'user_id'=>3
            ],
            [
                'id'=>6,
                'date'=>'2018-05-05',
                'amount'=>150000,
                'user_id'=>3
            ],
            [
                'id'=>7,
                'date'=>'2018-05-06',
                'amount'=>150000,
                'user_id'=>3
            ],
            [
                'id'=>8,
                'date'=>'2018-05-07',
                'amount'=>150000,
                'user_id'=>3
            ],
            [
                'id'=>9,
                'date'=>'2018-05-08',
                'amount'=>150000,
                'user_id'=>3
            ],
            [
                'id'=>10,
                'date'=>'2018-05-09',
                'amount'=>150000,
                'user_id'=>3
            ]
        ]);
    }
}

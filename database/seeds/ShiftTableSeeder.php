<?php

use Illuminate\Database\Seeder;

class ShiftTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shift')->insert([
            [
                'id' => 1,
                'date' => '2018-04-30',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 2,
                'date' => '2018-05-01',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 3,
                'date' => '2018-05-02',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 4,
                'date' => '2018-05-03',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 5,
                'date' => '2018-05-04',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 6,
                'date' => '2018-05-05',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 7,
                'date' => '2018-05-06',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 8,
                'date' => '2018-05-07',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 9,
                'date' => '2018-05-08',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 10,
                'date' => '2018-05-09',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 11,
                'date' => '2018-05-10',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 12,
                'date' => '2018-05-11',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 13,
                'date' => '2018-05-12',
                'shift_pattern_id' =>1,
                'shift_master_id'=>1
            ],
            [
                'id' => 14,
                'date' => '2018-04-30',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 15,
                'date' => '2018-05-01',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 16,
                'date' => '2018-05-02',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 17,
                'date' => '2018-05-03',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 18,
                'date' => '2018-05-04',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 19,
                'date' => '2018-05-05',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 20,
                'date' => '2018-05-06',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 21,
                'date' => '2018-05-07',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 22,
                'date' => '2018-05-08',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 23,
                'date' => '2018-05-09',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 24,
                'date' => '2018-05-10',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 25,
                'date' => '2018-05-11',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 26,
                'date' => '2018-05-12',
                'shift_pattern_id' =>2,
                'shift_master_id'=>2
            ],
            [
                'id' => 27,
                'date' => '2018-04-30',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 28,
                'date' => '2018-05-01',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 29,
                'date' => '2018-05-02',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 30,
                'date' => '2018-05-03',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 31,
                'date' => '2018-05-04',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 32,
                'date' => '2018-05-05',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 33,
                'date' => '2018-05-06',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 34,
                'date' => '2018-05-07',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 35,
                'date' => '2018-05-08',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 36,
                'date' => '2018-05-09',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 37,
                'date' => '2018-05-10',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 38,
                'date' => '2018-05-11',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ],
            [
                'id' => 39,
                'date' => '2018-05-12',
                'shift_pattern_id' =>3,
                'shift_master_id'=>3
            ]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
        [
            [   'id'=>'1',
                'name' => 'Admin',
                'password' => bcrypt('secret'),
                'address' => 'Hà Nội',
                'gender' => 1,
                'email' => 'admin@gmail.com',
                'avatar' => 'img/avatars/6.jpg',
                'status' => 1,
                'phone_number' => '01684736726',
                'date_of_birth' => Carbon::now(),
                'user_group_id' => null
            ],

            [   'id'=>'2',
                'name' => 'Nguyễn Văn A',
                'password' => bcrypt('secret'),
                'address' => 'Hà Nội',
                'gender' => 1,
                'email' => 'manager@gmail.com',
                'avatar' => str_random(10),
                'status' => 1,
                'phone_number' => '01684736726',
                'date_of_birth' => Carbon::now(),
                'user_group_id' => 2
            ],

            [   'id'=>'3',
                'name' => 'Lại Vân',
                'password' => bcrypt('secret'),
                'address' => 'Hà Nội',
                'gender' => 1,
                'email' => 'employee@gmail.com',
                'avatar' => 'img/avatars/2.jpg',
                'status' => 1,
                'phone_number' => '01253400545',
                'date_of_birth' => Carbon::now(),
                'user_group_id' => 2
            ],
            [   'id'=>'4',
                'name' => 'Trần An',
                'password' => bcrypt('secret'),
                'address' => 'Hà Nội',
                'gender' => 1,
                'email' => 'antran@gmail.com',
                'avatar' => 'img/avatars/6.jpg',
                'status' => 1,
                'phone_number' => '01253400545',
                'date_of_birth' => Carbon::now(),
                'user_group_id' => 2
            ]
        ]
        );
    }
}

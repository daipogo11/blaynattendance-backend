<?php

use Illuminate\Database\Seeder;

class ShiftPatternTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shift_pattern')->insert([
            [
                'id' => 1,
                'description' => 'Ca 1',
                'name' => 'Ca 1',
                'start_time'=>'7:00:00',
                'end_time'=>'12:00:00',
                'factor_salary'=>1
            ],
        	[
                'id' => 2,
                'description' => 'Ca 2',
                'name' => 'Ca 2',
                'start_time'=>'12:00:00',
                'end_time'=>'17:00:00',
                'factor_salary'=>1
            ],
            [
                'id' => 3,
                'description' => 'Ca 3',
                'name' => 'Ca 3',
                'start_time'=>'17:00:00',
                'end_time'=>'22:00:00',
                'factor_salary'=>1.2
            ]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class NotificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('notifications')->insert([
            [
                'id' => 1,
                'content' => 'Ngày 8/5/2018, yêu cầu nhân viên mặc áo trắng đi làm',
                'sent_id'=>1,
                'created_at'=>'2018-05-07 07:00:00',
                'updated_at'=>'2018-05-07 07:00:00'
            ],
        	[
                'id' => 2,
                'content' => 'Ngày 10/5/2018, yêu cầu nhân viên mặc áo trắng đi làm',
                'sent_id'=>1,
                'created_at'=>'2018-05-09 07:00:00',
                'updated_at'=>'2018-05-09 07:00:00'
            ]
        ]);
        DB::table('comments')->insert([
            [
                'id' => 1,
                'content' => 'Đã biết',
                'send_id'=>4,
                'created_at'=>'2018-05-07 12:00:00',
                'updated_at'=>'2018-05-07 12:00:00',
                'notification_id'=>1,
            ],
        	[
                'id' => 2,
                'content' => 'Đã biết',
                'send_id'=>4,
                'created_at'=>'2018-05-09 12:00:00',
                'updated_at'=>'2018-05-09 12:00:00',
                'notification_id'=>2,
            ]
        ]);

    }
}

<?php

use Illuminate\Database\Seeder;

class UserGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_groups')->insert([
            [
                'id' => 1,
                'name' => 'Quản lý',
                'wage_group_id' => 1
            ],
        	[
                'id' => 2,
                'name' => 'Nhân viên',
                'wage_group_id' => 2
            ]
        ]);
    }
}

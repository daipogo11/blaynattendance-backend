<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            [
            	['id' => 1, 'name' => 'admin'],
            	['id' => 2, 'name' => 'manager'],
            	['id' => 3, 'name' => 'employee']
            ]
        );

        DB::table('user_has_role')->insert(
            [
            	['user_id' => 1, 'role_id' => 1],
            	['user_id' => 3, 'role_id' => 3]
            ]
        );
    }
}

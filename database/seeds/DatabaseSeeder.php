<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(WageGroupTableSeeder::class);
    	$this->call(UserGroupTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(RequestTypeTableSeeder::class);
        $this->call(ShiftPatternTableSeeder::class);
        $this->call(ShiftMasterTableSeeder::class);
        $this->call(ShiftTableSeeder::class);
        $this->call(EmployeeShiftTableSeeder::class);
        $this->call(NotificationTableSeeder::class);
        $this->call(WorkedTimeTableSeeder::class);
    }
}

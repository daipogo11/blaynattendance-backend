<?php

use Illuminate\Database\Seeder;

class RequestTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('shift_request_type')->insert([
         	[
        		'id' => 1,
        		'name' => 'Đơn xin đổi ca'
        	],
        	[
        		'id' => 2,
        		'name' => 'Đơn xin nghỉ ca'
        	]
        ]);
    }
}

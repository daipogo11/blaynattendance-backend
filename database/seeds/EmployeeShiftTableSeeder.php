<?php

use Illuminate\Database\Seeder;

class EmployeeShiftTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employee_shift')->insert([
            [
                'employee_id' => 3,
                'shift_id'=>1,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>2,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>3,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>4,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>5,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>6,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>7,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>8,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>9,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>10,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>11,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>12,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>13,
            ],
            [
                'employee_id' => 3,
                'shift_id'=>37,
            ]        	
        ]);
    }
}

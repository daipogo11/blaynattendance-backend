<?php

use Illuminate\Database\Seeder;

class WageGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wage_groups')->insert([
        	[
                'id' => 1,
        	    'name' => 'Quản lý'
            ],
            [
                'id' => 2,
                'name' => 'Nhân viên'
            ]
        ]);
        DB::table('wage')->insert([
        	[
                'id' => 1,
                'salary' => '30000',
                'start_date' => '2018-04-24',
                'wage_group_id'=>'1'

            ],
            [
                'id' => 2,
                'salary' => '10000',
                'start_date' => '2018-04-30',
                'wage_group_id'=>'2'
            ]
        ]);
    }
}

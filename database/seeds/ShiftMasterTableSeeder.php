<?php

use Illuminate\Database\Seeder;

class ShiftMasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shift_master')->insert([
            [
                'id' => 1,
                'description' => 'Ca1',
                'start_date'=>'2018-04-30',
                'end_date'=>'2018-05-12',
                'shift_pattern_id'=>1
            ],
        	[
                'id' => 2,
                'description' => 'Ca2',
                'start_date'=>'2018-04-30',
                'end_date'=>'2018-05-12',
                'shift_pattern_id'=>2
            ],
            [
                'id' => 3,
                'description' => 'Ca3',
                'start_date'=>'2018-04-30',
                'end_date'=>'2018-05-12',
                'shift_pattern_id'=>3
            ],
        ]);
    }
}

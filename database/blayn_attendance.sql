-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema blayn_attendance
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema blayn_attendance
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `blayn_attendance` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
USE `blayn_attendance` ;

-- -----------------------------------------------------
-- Table `blayn_attendance`.`wage_groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`wage_groups` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`user_groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`user_groups` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `wage_groups_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_user_groups_wage_groups1_idx` (`wage_groups_id` ASC),
  CONSTRAINT `fk_user_groups_wage_groups1`
    FOREIGN KEY (`wage_groups_id`)
    REFERENCES `blayn_attendance`.`wage_groups` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`users` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `last_name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `username` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `password` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `email` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `gender` TINYINT(4) NULL DEFAULT NULL,
  `address` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `dateOfBirth` DATETIME NULL DEFAULT NULL,
  `avatar` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `status` TINYINT(4) NULL DEFAULT NULL,
  `user_groups_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `user_groups_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_users_user_groups_idx` (`user_groups_id` ASC),
  CONSTRAINT `fk_users_user_groups`
    FOREIGN KEY (`user_groups_id`)
    REFERENCES `blayn_attendance`.`user_groups` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`employee_shift`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`employee_shift` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `employee_id` INT(10) UNSIGNED NOT NULL,
  `shift_id` INT(11) NOT NULL,
  `created_date` DATETIME NOT NULL,
  `created_by` INT(11) NOT NULL,
  `last_modified_date` DATETIME NOT NULL,
  `last_modified_by` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_employee_shift_users1_idx` (`employee_id` ASC),
  INDEX `fk_employee_shift_shift_idx` (`shift_id` ASC),
  CONSTRAINT `fk_employee_shift_shift`
    FOREIGN KEY (`shift_id`)
    REFERENCES `blayn_attendance`.`shift` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_employee_shift_users1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `blayn_attendance`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = 'map employee (user) to shift';


-- -----------------------------------------------------
-- Table `blayn_attendance`.`shift_master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`shift_master` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(1024) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT 'further information',
  `start_date` DATETIME NOT NULL COMMENT 'start apply this shift.',
  `end_date` DATETIME NULL DEFAULT NULL COMMENT 'time to stop apply this shift, null -> no end date, apply forever',
  `created_date` DATETIME NOT NULL,
  `create_by` INT(11) NOT NULL,
  `last_modified_date` DATETIME NOT NULL,
  `last_modified_by` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`shift`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`shift` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `start_time` TIME NOT NULL COMMENT 'time to start the shift.',
  `end_time` TIME NOT NULL COMMENT 'time to end the shift.',
  `description` VARCHAR(1024) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT 'further information',
  `created_date` DATETIME NOT NULL,
  `created_by` INT(11) NOT NULL,
  `last_modified_date` DATETIME NOT NULL,
  `last_modified_by` INT(11) NOT NULL,
  `shift_master_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_shift_shift_master1_idx` (`shift_master_id` ASC),
  CONSTRAINT `fk_shift_shift_master1`
    FOREIGN KEY (`shift_master_id`)
    REFERENCES `blayn_attendance`.`shift_master` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = 'shift detail';


-- -----------------------------------------------------
-- Table `blayn_attendance`.`exception_shift`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`exception_shift` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `start_time` TIME NULL DEFAULT NULL COMMENT 'new time to start working, if null -> employee does not want to change the start working time, still use start time in shift that employee maps to.',
  `end_time` TIME NULL DEFAULT NULL COMMENT 'new time to end working, if null -> employee does not want to change the end working time, still use end time in shift that employee maps to.',
  `created_date` DATETIME NOT NULL,
  `created_by` INT(11) NULL DEFAULT NULL,
  `request_id` INT(11) NOT NULL,
  `employee_id` INT(10) UNSIGNED NOT NULL,
  `shift_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_exception_shift_users1_idx` (`employee_id` ASC),
  INDEX `fk_exception_shift_shift1_idx` (`shift_id` ASC),
  CONSTRAINT `fk_exception_shift_shift1`
    FOREIGN KEY (`shift_id`)
    REFERENCES `blayn_attendance`.`shift` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_exception_shift_users1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `blayn_attendance`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = 'exception shift after store manager approved the change request.\nif both start time, end time is null -> employee take leave';


-- -----------------------------------------------------
-- Table `blayn_attendance`.`finished_payroll_request`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`finished_payroll_request` (
  `id` INT(11) NOT NULL,
  `amount` DOUBLE NULL DEFAULT NULL,
  `time` DATETIME NULL DEFAULT NULL,
  `status` TINYINT(4) NULL DEFAULT NULL,
  `users_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_finished_pending_request_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_finished_pending_request_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `blayn_attendance`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`finished_shift_request`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`finished_shift_request` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `start_time` TIME NULL DEFAULT NULL COMMENT 'new working start time.',
  `end_time` TIME NULL DEFAULT NULL COMMENT 'new working end time.',
  `description` VARCHAR(1024) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT 'further information',
  `shift_id` INT(11) NOT NULL COMMENT 'the shift that will be changed.',
  `created_date` DATETIME NOT NULL,
  `created_by` INT(11) NOT NULL,
  `employee_id` INT(10) UNSIGNED NOT NULL,
  `status` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_finished_request_users1_idx` (`employee_id` ASC),
  INDEX `fk_finished_request_shift_idx` (`shift_id` ASC),
  CONSTRAINT `fk_finished_request_shift`
    FOREIGN KEY (`shift_id`)
    REFERENCES `blayn_attendance`.`shift` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_finished_request_users1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `blayn_attendance`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = 'accepted/declined change request.';


-- -----------------------------------------------------
-- Table `blayn_attendance`.`notifications`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`notifications` (
  `id` INT(11) NOT NULL,
  `content` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `received_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_notifications_users1_idx` (`received_id` ASC),
  CONSTRAINT `fk_notifications_users1`
    FOREIGN KEY (`received_id`)
    REFERENCES `blayn_attendance`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`payroll`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`payroll` (
  `id` INT(11) NOT NULL,
  `start_date` DATE NULL DEFAULT NULL,
  `end_date` DATE NULL DEFAULT NULL,
  `amount` DOUBLE NULL DEFAULT NULL,
  `users_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_payroll_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_payroll_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `blayn_attendance`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`pending_payroll_request`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`pending_payroll_request` (
  `id` INT(11) NOT NULL,
  `amount` DOUBLE NULL DEFAULT NULL,
  `time` DATETIME NULL DEFAULT NULL,
  `users_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_pending_payroll_request_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_pending_payroll_request_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `blayn_attendance`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`pending_shift_request`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`pending_shift_request` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `start_time` TIME NULL DEFAULT NULL COMMENT 'new working start time.',
  `end_time` TIME NULL DEFAULT NULL COMMENT 'new working end time.',
  `shift_id` INT(11) NOT NULL COMMENT 'shift that employee want to change.',
  `created_date` DATETIME NOT NULL,
  `request_type` INT(11) NOT NULL DEFAULT '0' COMMENT 'request type: 0 - change time request, 1 - take leave request',
  `description` VARCHAR(1024) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT 'further information',
  `employee_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_pending_request_users1_idx` (`employee_id` ASC),
  INDEX `fk_pending_request_shift_idx` (`shift_id` ASC),
  CONSTRAINT `fk_pending_request_shift`
    FOREIGN KEY (`shift_id`)
    REFERENCES `blayn_attendance`.`shift` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pending_request_users1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `blayn_attendance`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = 'all pending request';


-- -----------------------------------------------------
-- Table `blayn_attendance`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`role` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`store`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`store` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `address` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`user_has_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`user_has_role` (
  `role_id` INT(11) NOT NULL,
  `users_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `users_id`),
  INDEX `fk_role_has_users_users1_idx` (`users_id` ASC),
  INDEX `fk_role_has_users_role1_idx` (`role_id` ASC),
  CONSTRAINT `fk_role_has_users_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `blayn_attendance`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_has_users_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `blayn_attendance`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`wage`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`wage` (
  `id` INT(11) NOT NULL,
  `salary` DOUBLE NULL DEFAULT NULL,
  `start_date` DATE NULL DEFAULT NULL,
  `wage_groups_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_wage_wage_groups1_idx` (`wage_groups_id` ASC),
  CONSTRAINT `fk_wage_wage_groups1`
    FOREIGN KEY (`wage_groups_id`)
    REFERENCES `blayn_attendance`.`wage_groups` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `blayn_attendance`.`worked_time`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blayn_attendance`.`worked_time` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `in_time` DATETIME NULL DEFAULT NULL,
  `out_time` DATETIME NULL DEFAULT NULL,
  `employee_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_worked_time_users1_idx` (`employee_id` ASC),
  CONSTRAINT `fk_worked_time_users1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `blayn_attendance`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = 'autual workd time of employee (check-in, check-out)';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

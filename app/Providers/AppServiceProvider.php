<?php

namespace App\Providers;

use App;

use App\Repositories\UserGroup\UserGroupInterface;
use App\Repositories\UserGroup\UserGroupRepository;

use App\Repositories\User\UserInterface;
use App\Repositories\User\UserRepository;

use App\Repositories\WageGroup\WageGroupInterface;
use App\Repositories\WageGroup\WageGroupRepository;

use App\Repositories\Wage\WageInterface;
use App\Repositories\Wage\WageRepository;

use App\Repositories\PendingShiftRequest\PendingShiftRequestInterface;
use App\Repositories\PendingShiftRequest\PendingShiftRequestRepository;

use App\Repositories\PendingPayrollRequest\PendingPayrollRequestInterface;
use App\Repositories\PendingPayrollRequest\PendingPayrollRequestRepository;

use App\Repositories\FinishedShiftRequest\FinishedShiftRequestInterface;
use App\Repositories\FinishedShiftRequest\FinishedShiftRequestRepository;

use App\Repositories\FinishedPayrollRequest\FinishedPayrollRequestInterface;
use App\Repositories\FinishedPayrollRequest\FinishedPayrollRequestRepository;

use App\Repositories\Payroll\PayrollInterface;
use App\Repositories\Payroll\PayrollRepository;

use App\Repositories\Notification\NotificationInterface;
use App\Repositories\Notification\NotificationRepository;

use App\Repositories\Store\StoreInterface;
use App\Repositories\Store\StoreRepository;

use App\Repositories\Role\RoleInterface;
use App\Repositories\Role\RoleRepository;

use App\Repositories\Shift\ShiftInterface;
use App\Repositories\Shift\ShiftRepository;

use App\Repositories\ShiftMaster\ShiftMasterInterface;
use App\Repositories\ShiftMaster\ShiftMasterRepository;

use App\Repositories\WorkedTime\WorkedTimeInterface;
use App\Repositories\WorkedTime\WorkedTimeRepository;

use App\Repositories\ExceptionShift\ExceptionShiftInterface;
use App\Repositories\ExceptionShift\ExceptionShiftRepository;

use App\Repositories\ShiftPattern\ShiftPatternInterface;
use App\Repositories\ShiftPattern\ShiftPatternRepository;

use App\Repositories\Feedback\FeedbackInterface;
use App\Repositories\Feedback\FeedbackRepository;

use App\Repositories\PendingLeaveRequest\PendingLeaveRequestInterface;
use App\Repositories\PendingLeaveRequest\PendingLeaveRequestRepository;

use App\Repositories\FinishedLeaveRequest\FinishedLeaveRequestInterface;
use App\Repositories\FinishedLeaveRequest\FinishedLeaveRequestRepository;

use App\Repositories\Comment\CommentInterface;
use App\Repositories\Comment\CommentRepository;

use App\Repositories\Message\MessageInterface;
use App\Repositories\Message\MessageRepository;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind(UserGroupInterface::class, UserGroupRepository::class);
        App::bind(UserInterface::class, UserRepository::class);
        App::bind(WageGroupInterface::class, WageGroupRepository::class);
        App::bind(WageInterface::class, WageRepository::class);
        App::bind(RoleInterface::class, RoleRepository::class);
        App::bind(ShiftInterface::class, ShiftRepository::class);
        App::bind(StoreInterface::class, StoreRepository::class);
        App::bind(PayrollInterface::class, PayrollRepository::class);
        App::bind(NotificationInterface::class, NotificationRepository::class);
        App::bind(ShiftMasterInterface::class, ShiftMasterRepository::class);
        App::bind(ExceptionShiftInterface::class, ExceptionShiftRepository::class);
        App::bind(WorkedTimeInterface::class, WorkedTimeRepository::class);
        App::bind(PendingPayrollRequestInterface::class, PendingPayrollRequestRepository::class);
        App::bind(FinishedPayrollRequestInterface::class, FinishedPayrollRequestRepository::class);
        App::bind(PendingShiftRequestInterface::class, PendingShiftRequestRepository::class);
        App::bind(FinishedShiftRequestInterface::class, FinishedShiftRequestRepository::class);
        App::bind(ShiftPatternInterface::class, ShiftPatternRepository::class);
        App::bind(FeedbackInterface::class, FeedbackRepository::class);
        App::bind(PendingLeaveRequestInterface::class, PendingLeaveRequestRepository::class);
        App::bind(FinishedLeaveRequestInterface::class, FinishedLeaveRequestRepository::class);
        App::bind(CommentInterface::class, CommentRepository::class);
        App::bind( MessageInterface::class,  MessageRepository::class);
    }
}

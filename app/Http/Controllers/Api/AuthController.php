<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use JWTFactory;
use JWTAuth;

class AuthController extends Controller
{
    public function loginAdmin(Request $request) {
    	$validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password'=> 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid email or password'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could not create token'], 500);
        }

        $user = auth()->user();

        if (! $user->hasAnyRoles(['admin', 'manager']) || ! $user->status == 1) {
//            JWTAuth::invalidate($token);
            return response()->json(['error' => true], 403);
        }

        return response()->json(compact('token', 'user'));
    }

    public function loginEmployee(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password'=> 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid email or password'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could not create token'], 500);
        }

        $user = auth()->user();

        if (! $user->hasAnyRoles(['employee'])  || ! $user->status == 1) {
//            JWTAuth::invalidate($token);
            return response()->json(['error' => true], 403);
        }

        return response()->json(compact('token', 'user'));
    }

    public function getCurrentUser(Request $request) {
    	return response()->json([
    		'error' => false,
    		'data' => auth()->user()
    	]);
    }

    public function logout(Request $request) {
    	$this->validate($request, ['token' => 'required']);

        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['message'=> "You have successfully logged out."]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'Failed to logout, please try again.'], 500);
        }
    }
}

<?php

namespace App\Http\Controllers\Api\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Notification\NotificationInterface;
use App\Repositories\User\UserInterface;
use App\Repositories\Comment\CommentInterface;
use Validator;

class NotificationController extends Controller
{
    protected $notificationRepository;
    protected $userRepository;
    protected $commentRepository;

    public function __construct(
        UserInterface $userRepository,
        NotificationInterface $notificationRepository,
        CommentInterface $commentRepository
    ) {
        $this->userRepository = $userRepository;
        $this->notificationRepository = $notificationRepository;
        $this->commentRepository = $commentRepository;
    }

    public function getNotification() {
        try {
            $notification =  $this->notificationRepository->orderBy('created_at', 'desc')->get();
            foreach ($notification as $item) {
                $item['sent_name'] = $item->receive()->pluck('name')[0];
                $item['sent_avatar'] = $item->receive()->pluck('avatar')[0];
            }
            return response()->json(compact('notification'), 200);

        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function getComment(){
        try{
            $comment = $this->commentRepository->all();
            foreach ($comment as $item) {
                $item['send_name'] = $item->receive()->pluck('name')[0];
                $item['send_avatar'] = $item->receive()->pluck('avatar')[0];
            }
            return response()->json(compact('comment',200));
        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
    public function postComment(Request $request){
        $validator = Validator::make($request->all(), [
            'content'=>'required',
            'notification_id'=>'required'

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $insertData = $request->only(['notification_id','content']);
        $insertData['send_id'] = auth()->user()->id;
        try {
            $commentRequest = $this->commentRepository->create($insertData);
            return response()->json(compact(('commentRequest')), 200);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
    public function postNotification(Request $request){
        $validator = Validator::make($request->all(), [
            'content'=>'required'

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $insertData = $request->only(['content']);
        $insertData['sent_id'] = auth()->user()->id;
        try {
            $notificationRequest = $this->notificationRepository->create($insertData);
            return response()->json(compact(('notificationRequest')), 200);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}

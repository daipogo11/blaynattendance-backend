<?php

namespace App\Http\Controllers\Api\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Shift\ShiftInterface;
use App\Repositories\ShiftMaster\ShiftMasterInterface;
use App\Repositories\ShiftPattern\ShiftPatternInterface;
use App\Repositories\User\UserInterface;
use Validator;
use Carbon\Carbon;
use DateTime;
use DatePeriod;
use DateInterval;

class ShiftController extends Controller
{
    protected $shiftMasterRepository;
    protected $shiftRepository;
    protected $shiftPatternRepository;
    protected $userRepository;

    public function __construct(ShiftInterface $shiftRepository, ShiftMasterInterface $shiftMasterRepository, ShiftPatternInterface $shiftPatternRepository, UserInterface $userRepository) {
        $this->shiftRepository = $shiftRepository;
        $this->shiftMasterRepository = $shiftMasterRepository;
        $this->shiftPatternRepository = $shiftPatternRepository;
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shift_masters = $this->shiftMasterRepository->all();

        foreach ($shift_masters as $item) {
            $item['shift_pattern'] = $item->shift_pattern()->pluck('name')[0];
        }

        return response()->json(compact('shift_masters'), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'required',
            'end_date' => 'required|after:start_date',
            'shift_pattern_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $start_date = date('Y-m-d', strtotime($request->start_date));
        $end_date = date('Y-m-d', strtotime($request->end_date));

        $insertData = $request->only(['shift_pattern_id']);
        $insertData['start_date'] = $start_date;
        $insertData['end_date'] = $end_date;
        try {
            $shift_master = $this->shiftMasterRepository->firstOrCreate($insertData);
            $shift_master['shift_pattern'] = $shift_master->shift_pattern()->pluck('name')[0];
            $insertData['shift_master_id'] = $shift_master->id;

            $begin = new DateTime($start_date);
            $end = new DateTime($end_date);
            $end = $end->modify('+1 day');

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);

            foreach ($period as $dt) {
                $insertData['date'] = $dt->format("Y-m-d");
                $this->shiftRepository->create($insertData);
            }
        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('shift_master'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'required',
            'end_date' => 'required|after:start_date',
            'shift_pattern_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $start_date = date('Y-m-d', strtotime($request->start_date));
        $end_date = date('Y-m-d', strtotime($request->end_date));

        $updateData = $request->only(['shift_pattern_id']);
        $updateData['start_date'] = $start_date;
        $updateData['end_date'] = $end_date;
        
        try {
            if(! $this->shiftMasterRepository->find($id)) {
                return response()->json([
                    'error' => 'Not Found'
                ], 400);
            } else {
                $this->shiftMasterRepository->update($id, $updateData);
                $shift_master = $this->shiftMasterRepository->find($id);
                $shift_master['shift_pattern'] = $shift_master->shift_pattern()->pluck('name')[0];

                $shifts = $this->shiftRepository->get()->where('shift_master_id', $id);
                foreach ($shifts as $shift) {
                    $this->shiftRepository->delete($shift->id);
                }

                $updateData['shift_master_id'] = $id;
                $begin = new DateTime($start_date);
                $end = new DateTime($end_date);
                $end = $end->modify('+1 day');

                $interval = DateInterval::createFromDateString('1 day');
                $period = new DatePeriod($begin, $interval, $end);

                foreach ($period as $dt) {
                    $updateData['date'] = $dt->format("Y-m-d");
                    $this->shiftRepository->create($updateData);
                }
            }
        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('shift_master'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $shift_master = $this->shiftMasterRepository->find($id);

            if(! $shift_master) {
                return response()->json([
                    'error' => true,
                    'message' => 'Not Found'
                ], 400);
            }

            $this->shiftMasterRepository->delete($id);

        }   catch (Exception $e) {
            response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('shift_master'), 200);
    }

    public function getShifts(Request $request) {
        $shifts = $this->shiftRepository->select('date', 'shift_pattern_id')->distinct()->get();

        if($request->today) {
            $shifts = $shifts->filter(function($item) {
                $today = Carbon::today()->format('Y-m-d');
                return $item->date == $today;
            })->values()->all();
        }

        foreach ($shifts as $shift) {
            $shift['id'] = $shift->shift_pattern->shifts()->whereDate('date', $shift->date)->first()->id;;
            $shift_pattern = $shift->shift_pattern();
            $shift['start_time'] = $shift_pattern->pluck('start_time')[0];
            $shift['end_time'] = $shift_pattern->pluck('end_time')[0];
            $shift['name'] = $shift_pattern->pluck('name')[0];
            $shift['employees'] = $shift->employees()->pluck('id');
        }

        return response()->json(compact('shifts'), 200);
    }

    public function showShift($id) {
        $shift = $this->shiftRepository->find($id);
        $shift['name'] = $shift->shift_pattern()->pluck('name')[0];
        return response()->json(compact('shift'), 200);
    }

    public function scheduleShift(Request $request) {
        $validator = Validator::make($request->all(), [
            'employees' => 'required',
            'shift_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        try {
            $shift_id = $request->shift_id;
            $employees = $request->employees;

            $shift = $this->shiftRepository->find($shift_id);

            if(! $shift) {
                return response()->json([
                    'error' => true,
                    'message' => 'shift does not exist'
                ], 400);
            }

            $shift->employees()->detach();
            if (!is_array($employees)) $shifts->employees()->attach($employees);
            else {
                    foreach ($employees as $employee_id) {
                    $shift->employees()->attach($employee_id);
                }
            }
            
            $shift['employees'] = $shift->employees()->pluck('name');
            return response()->json(compact('shift'), 200);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 200);
        }
    }
}

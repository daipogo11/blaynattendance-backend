<?php

namespace App\Http\Controllers\Api\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WorkedTime\WorkedTimeInterface;
use App\Repositories\Shift\ShiftInterface;
use Validator;
use Carbon\Carbon;

class AttendanceController extends Controller
{
    protected $workedTimeRepository;
    protected $shiftRepository;

    public function __construct(WorkedTimeInterface $workedTimeRepository, ShiftInterface $shiftRepository) {
    	$this->workedTimeRepository = $workedTimeRepository;
        $this->shiftRepository = $shiftRepository;
    }

    public function index() {
        $today = Carbon::today()->format('Y-m-d');
        $workedTimes = $this->workedTimeRepository->whereDate('in_time', $today)->get();
        foreach ($workedTimes as $item) {
            $item['employee'] = $item->employee()->pluck('name')[0];
            $item['shift_name'] = $item->shift->shift_pattern()->pluck('name')[0];
        }

        return response()->json(compact('workedTimes'), 200);
    }

    public function check_in(Request $request) {
    	$validator = Validator::make($request->all(), [
    		"employee_id" => "required|integer",
            "shift_id" => "required|integer"
    	]);

    	if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $insertData = $request->only(['employee_id', 'shift_id']);
        $insertData += ["in_time" => Carbon::now()];
        $employee_id = $request->employee_id;
        $shift_id = $request->shift_id;

        try {
            if(!$this->shiftRepository->find($shift_id)->employees->contains('id', $employee_id)) {
                return response()->json([
                    'error' => 'Employee does not have this shift'
                ], 500);
            }

        	if($workedTime = $this->workedTimeRepository->get()->where('employee_id', $employee_id)->where('shift_id', $shift_id)->first())
            {
                return response()->json([
                    'error' => 'Employee checked in this shift'
                ], 500);
            }

            $shift_end_time = $this->shiftRepository->find($shift_id)->shift_pattern()->pluck('end_time')[0];
            $shift_start_time = $this->shiftRepository->find($shift_id)->shift_pattern()->pluck('start_time')[0];
            $shift_end_time = date('H:i:s', strtotime($shift_end_time));
            $shift_start_time = date('H:i:s', strtotime($shift_start_time));

            $in_time = date('H:i:s', strtotime($insertData["in_time"]));
            if($in_time > $shift_end_time) {
                return response()->json([
                    'error' => 'This shift ended'
                ], 500);
            }

            if($in_time < $shift_start_time) {
                return response()->json([
                    'error' => 'This shift does not start'
                ], 500);
            }

            $workedTime = $this->workedTimeRepository->create($insertData);
            $workedTime = $this->workedTimeRepository->find($workedTime->id);
            $workedTime['employee'] = $workedTime->employee()->pluck('name')[0];
            $workedTime['shift_name'] = $workedTime->shift->shift_pattern()->pluck('name')[0];
        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
        return response()->json(compact('workedTime'), 200);
    }

    public function check_out(Request $request) {
    	$validator = Validator::make($request->all(), [
    		"employee_id" => "required|integer",
            "shift_id" => "required|integer"
    	]);

    	if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        try {
           	$workedTime = $this->workedTimeRepository->get()->where('employee_id', $request->employee_id)->where('shift_id', $request->shift_id)->first();
            if(!$workedTime ) {
                return response()->json([
                    "error" => "Employee did not check in"
                ], 500);
            } else {
                if (!is_null($workedTime->out_time)) {
                    return response()->json([
                        "error" => "Employee is checked out this shift"
                    ], 500);
                }
                $workedTime->out_time = Carbon::now();
                $workedTime->save();
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        $workedTime = $this->workedTimeRepository->find($workedTime->id);
        return response()->json(compact('workedTime'), 200);
    }

    public function getEmployeeToday() {

    }
}

<?php

namespace App\Http\Controllers\Api\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PendingPayrollRequest\PendingPayrollRequestInterface as PendingPayrollRequest;
use App\Repositories\FinishedPayrollRequest\FinishedPayrollRequestInterface as FinishedPayrollRequest;
use App\Repositories\PendingShiftRequest\PendingShiftRequestInterface as PendingShiftRequest;
use App\Repositories\FinishedShiftRequest\FinishedShiftRequestInterface as FinishedShiftRequest;
use App\Repositories\PendingLeaveRequest\PendingLeaveRequestInterface as PendingLeaveRequest;
use App\Repositories\FinishedLeaveRequest\FinishedLeaveRequestInterface as FinishedLeaveRequest;
use App\Repositories\Feedback\FeedbackInterface;
use App\Repositories\Shift\ShiftInterface;
use Validator;

class RequestController extends Controller
{
    protected $pendingPayrollRequestRepository;
    protected $finishedPayrollRequestRepository;
    protected $pendingShiftRequestRepository;
    protected $finishedShiftRequestRepository;
    protected $pendingLeaveRequestRepository;
    protected $finishedLeaveRequestRepository;
    protected $feedbackRepository; 
    protected $shiftRepository;

    public function __construct(PendingPayrollRequest $pendingPayrollRequestRepository,
    							FinishedPayrollRequest $finishedPayrollRequestRepository,
    							PendingShiftRequest $pendingShiftRequestRepository,
    							FinishedShiftRequest $finishedShiftRequestRepository,
    							PendingLeaveRequest $pendingLeaveRequestRepository,
    							FinishedLeaveRequest $finishedLeaveRequestRepository,
    							FeedbackInterface $feedbackRepository,
    							ShiftInterface $shiftRepository)
    {
    	$this->pendingPayrollRequestRepository = $pendingPayrollRequestRepository;
    	$this->finishedPayrollRequestRepository = $finishedPayrollRequestRepository;
    	$this->pendingShiftRequestRepository = $pendingShiftRequestRepository;
    	$this->finishedShiftRequestRepository = $finishedShiftRequestRepository;
    	$this->pendingLeaveRequestRepository = $pendingLeaveRequestRepository;
    	$this->finishedLeaveRequestRepository = $finishedLeaveRequestRepository;
    	$this->feedbackRepository = $feedbackRepository;
    	$this->shiftRepository = $shiftRepository;
    }

    public function index() {
    	$requests = array();

    	$payrollRequest = $this->pendingPayrollRequestRepository->all();
    	foreach ($payrollRequest as $item) {
    		$item['user'] = $item->user()->pluck('name')[0];
    	}

    	$shiftRequest = $this->pendingShiftRequestRepository->all();
    	foreach ($shiftRequest as $item) {
    		$item['user'] = $item->employee()->pluck('name')[0];
    		$item['date'] = $this->shiftRepository->find($item->old_shift_id)->date;
    		$item['old_shift'] = $this->shiftRepository->find($item->old_shift_id)->shift_pattern()->pluck('name')[0];
    		if(!is_null($item->shift_change_id)) 
    			$item['shift_change'] = $this->shiftRepository->find($item->shift_change_id)->shift_pattern()->pluck('name')[0];
    	}

    	$leaveRequest = $this->pendingLeaveRequestRepository->all();
    	foreach ($leaveRequest as $item) {
    		$item['user'] = $item->user()->pluck('name')[0];
    	}

    	$feedback = $this->feedbackRepository->all();
    	foreach ($feedback as $item) {
    		$item['user'] = $item->user()->pluck('name')[0];
    	}

    	$requests['payrollRequest'] = $payrollRequest;
    	$requests['shiftRequest'] = $shiftRequest;
    	$requests['leaveRequest'] = $leaveRequest;
    	$requests['feedback'] = $feedback;

    	return response()->json(compact('requests'), 200);
    }

    public function finishedPayrollRequest(Request $request) {
    	$validator = Validator::make($request->all(), [
    		'request_id' => 'required',
    		'status' => 'required'
    	]);

    	if($validator->fails()) {
    		return response()->json($validator->errors());
    	}
    }
}

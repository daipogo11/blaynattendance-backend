<?php

namespace App\Http\Controllers\Api\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Shift\ShiftInterface;
use App\Repositories\ShiftMaster\ShiftMasterInterface;
use App\Repositories\ShiftPattern\ShiftPatternInterface;
use Validator;
use Carbon\Carbon;

class ShiftPatternController extends Controller
{
    protected $shiftMasterRepository;
    protected $shiftRepository;
    protected $shiftPatternRepository;

    public function __construct(ShiftInterface $shiftRepository, ShiftMasterInterface $shiftMasterRepository, ShiftPatternInterface $shiftPatternRepository) {
        $this->shiftRepository = $shiftRepository;
        $this->shiftMasterRepository = $shiftMasterRepository;
        $this->shiftPatternRepository = $shiftPatternRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shift_patterns = $this->shiftPatternRepository->all();

        return response()->json(compact('shift_patterns'), 200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'factor_salary' => 'required',
            'start_time' => 'required',
            'end_time' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $insertData = $request->only(['name','factor_salary']);
        $insertData['start_time'] = date('H:i:s', strtotime($request->start_time));
        $insertData['end_time'] = date('H:i:s', strtotime($request->end_time));
        try {
            $shift_pattern = $this->shiftPatternRepository->firstOrCreate($insertData);
        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('shift_pattern'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$shift_pattern = $this->shiftPatternRepository->find($id)) {
            return response()->json([
                'error' => true,
                'message' => 'shift pattern does not exist'
            ], 400);
        } else {
            // $shift['start_date'] = $shift->shift_master()->pluck('start_date')[0];
            // $shift['end_date'] = $shift->shift_master()->pluck('end_date')[0];
            return response()->json(compact('shift_pattern'), 200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'factor_salary' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $updateData = $request->only(['name','factor_salary']);

        $updateData['start_time'] = date('H:i:s', strtotime($request->start_time));
        $updateData['end_time'] = date('H:i:s', strtotime($request->end_time));
        try {
            if(! $this->shiftPatternRepository->find($id)) {
                return response()->json([
                    'error' => 'shift pattern does not exist'
                ], 400);
            } else {               
                // $updateData['shift_master_id'] = $shift_master->id;
                $this->shiftPatternRepository->update($id, $updateData);
                $shift_pattern = $this->shiftPatternRepository->find($id);
                // $shift['shift_master'] = $shift_master;
            }
        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('shift_pattern'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $shift_pattern = $this->shiftPatternRepository->find($id);

            if(! $shift_pattern) {
                return response()->json([
                    'error' => true,
                    'message' => 'shift pattern does not exist'
                ], 400);
            }

            $this->shiftPatternRepository->delete($id);

        }   catch (Exception $e) {
            response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('shift_pattern'), 200);
    }
}

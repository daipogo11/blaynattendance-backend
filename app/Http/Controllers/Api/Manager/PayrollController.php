<?php

namespace App\Http\Controllers\Api\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Repositories\Payroll\PayrollInterface;
use App\Repositories\WorkedTime\WorkedTimeInterface;

class PayrollController extends Controller
{
    protected $workedTimeRepository;
    protected $payrollRepository;

    public function __construct(PayrollInterface $payrollRepository, WorkedTimeInterface $workedTimeRepository) 
    {
    	$this->workedTimeRepository = $workedTimeRepository;
    	$this->payrollRepository = $payrollRepository;
    }

    public function index(Request $request) {
    	$month = $request->month;
    	$year = $request->year;

    	if(!is_null($month) && !is_null($year)) {
    		$payrolls = $this->payrollRepository->whereMonth('date', $month)
    								->whereYear('date', $year)
    								->groupBy('user_id')
    								->select('user_id', DB::raw('sum(amount) as total'))
    								->get();
    		foreach ($payrolls as $item) {
    			$item['user'] = $item->user()->pluck('name')[0];
    		}
    	} else {
    		return response()->json([
    			'error' => 'Please select month and year'
    		], 500);
    	}

    	return response()->json(compact('payrolls'), 200);
    }

    public function payrollEmployee($id, Request $request) {
    	$month = $request->month;
    	$year = $request->year;

    	if(!is_null($month) && !is_null($year)) {
    		$payrolls = $this->payrollRepository->whereMonth('date', $month)
    								->whereYear('date', $year)
    								->where('user_id', $id)
    								->get();
    		foreach ($payrolls as $item) {
    			$item['user'] = $item->user()->pluck('name')[0];
    		}
    	} else {
    		return response()->json([
    			'error' => 'Please select month and year'
    		], 500);
    	}

    	return response()->json(compact('payrolls'), 200);
    }
}

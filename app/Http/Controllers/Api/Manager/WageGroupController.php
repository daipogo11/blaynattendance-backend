<?php

namespace App\Http\Controllers\Api\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WageGroup\WageGroupInterface;
use App\Repositories\Wage\WageInterface;
use Validator;
use Carbon\Carbon;
use App\Models\Wage;

class WageGroupController extends Controller
{
    protected $wageGroupRepository;
    protected $wageRepository;

    public function __construct(WageGroupInterface $wageGroupRepository, WageInterface $wageRepository) {
        $this->wageGroupRepository = $wageGroupRepository;
        $this->wageRepository = $wageRepository;
    }

    /**
    * @SWG\Get(
    *      path="/wage-groups",
    *      operationId="getWageGroupsList",
    *      tags={"Wage Groups"},
    *      summary="Get list of wage groups",
    *      description="Returns list of wage groups",
    *      @SWG\Response(
    *          response=200,
    *          description="successful operation"
    *       ),
    *       @SWG\Response(response=400, description="Bad request"),
    *       security={
    *           {"api_key_security_example": {}}
    *       }
    *     )
    *
    * Returns list of wage groups
    */
    public function index()
    {
        $wageGroups = $this->wageGroupRepository->all();
        foreach ($wageGroups as $wageGroup) {
            $wage = $wageGroup->getCurrentWage();
            $wageGroup["wage"] = $wage;
        }

        return response()->json(compact('wageGroups'), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $insertData = $request->only(['name']);

        $insertWageData = $request->only(['salary']);
        $insertWageData += ["start_date" => date('Y-m-d', strtotime($request->start_date))];

        try {
            $wageGroup = $this->wageGroupRepository->create($insertData);
            $insertWageData += ["wage_group_id" => $wageGroup->id];
            $wage = $this->wageRepository->create($insertWageData);
            $wage = $wageGroup->getCurrentWage();
            $wageGroup["wage"] = $wage;

        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('wageGroup'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $wageGroup = $this->wageGroupRepository->find($id);

            if(! $wageGroup) {
                return response()->json([
                    'error' => true,
                    'message' => 'wage group does not exist'
                ], 400);
            } else {
                $wage = $wageGroup->getCurrentWage();
                $wageGroup["wage"] = $wage;
            }
        }   catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('wageGroup'), 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $updateData = $request->only(['name']);

        $insertWageData = $request->only(['salary']);
        $insertWageData += ["start_date" => date('Y-m-d', strtotime($request->start_date))];
        $insertWageData += ["wage_group_id" => $id];

        try {
            if(! $this->wageGroupRepository->find($id)) {
                return response()->json([
                    'error' => true,
                    'message' => 'wage group does not exist'
                ], 400);
            } else {
                $this->wageGroupRepository->update($id, $updateData);
                if (!$wage = $this->wageRepository->get()->where('start_date', $insertWageData['start_date'])->where('wage_group_id', $id)->first())
                {
                    $this->wageRepository->create($insertWageData);
                } else {
                    $this->wageRepository->update($wage->id, $insertWageData);
                }
                
            }
        }   catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        $wageGroup =  $this->wageGroupRepository->find($id);
        $wage = $wageGroup->getCurrentWage();
        $wageGroup["wage"] = $wage;

 
        return response()->json(compact('wageGroup'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $wageGroup = $this->wageGroupRepository->find($id);

            if(! $wageGroup) {
                return response()->json([
                    'error' => true,
                    'message' => 'wage group does not exist'
                ], 400);
            }

            $this->wageGroupRepository->delete($id);

        }   catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('wageGroup'), 200);
    }

    public function getStory() {
        try {
            $wageStory = $this->wageRepository->orderBy('start_date', 'desc')->orderBy('created_at', 'desc')->get();
            foreach ($wageStory as $item) {
                $item['wage_group'] = $item->wage_group()->pluck('name')[0];
            }

            return response()->json(compact('wageStory'), 200);

        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}

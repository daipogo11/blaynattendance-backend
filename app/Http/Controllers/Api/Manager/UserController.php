<?php

namespace App\Http\Controllers\Api\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserInterface;
use App\Repositories\Role\RoleInterface;
use App\Repositories\Shift\ShiftInterface;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Collection;

class UserController extends Controller
{
    protected $userRepository;
    protected $roleRepository;
    protected $shiftRepository;

    public function __construct(UserInterface $userRepository, RoleInterface $roleRepository, ShiftInterface $shiftRepository) {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->shiftRepository = $shiftRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //lấy tất cả user
        $users = $this->userRepository->select('id', 'name', 'address','phone_number', 'email', 'gender', 'date_of_birth', 'user_group_id', 'status')->get();

        //Lọc user ko có role admin
        $users = $users->filter(function($item) {
            return !$item->hasRole('admin');
        })->values()->all();

        //Nếu request có today -> lọc tất cả user có ca trong ngày hôm nay
        if($request->today) {
            $users = collect($users)->filter(function($item) {
                $today = Carbon::today()->format('Y-m-d');
                return $item->shifts->whereIn('date', $today)->isNotEmpty();
            })->values()->all();
        }

        foreach ($users as $user) {
            $user_group = $user->user_group()->pluck('name');

            if ($user_group->isEmpty()) {
                $user['user_group'] = null;
            } else {
                $user["user_group"] = $user_group[0];
            }

            $user['shifts'] = $user->shifts()->pluck('id');
        }

        return response()->json(compact('users'), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'password' => 'required|string',
            'address' => 'required|string',
            'gender' => 'required|integer',
            'email' => 'required|string',
            'avatar' => 'image',
            'status' => 'required',
            'phone_number' => 'required|string',
            'date_of_birth' => 'required|date',
            'user_group_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $insertData = $request->only([  'name',
                                        'address',
                                        'gender',
                                        'email',
                                        'status',
                                        'phone_number',
                                        'user_group_id'
                                    ]);

        $insertData += ["date_of_birth" => date('Y-m-d', strtotime($request->date_of_birth))];
        
        if ($request->hasFile('avatar')) {
            $path = $request->file('avatar')->store('public');
            $insertData += ["avatar" => substr($path, 7)];
        } 
        
        $insertData += ["password" => bcrypt($request->password)];

        try {
            $user = $this->userRepository->create($insertData);
            $role = $this->roleRepository->get()->where('name', 'employee')->first();
            $user->roles()->attach($role->id);
        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        $user["user_group"] = $user->user_group()->pluck("name")[0];
        return response()->json(compact('user'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = $this->userRepository->find($id);

            if(! $user) {
                return response()->json([
                    'error' => true,
                    'message' => 'User does not exist'
                ], 400);
            } else {
                $user["user_group"] = $user->user_group()->pluck("name")[0];
            }
        }   catch (Exception $e) {
            response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('user'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'address' => 'required|string',
            'gender' => 'required|integer',
            'email' => 'required|string|unique:users,email,' . $id,
            'avatar' => 'image',
            'status' => 'required|integer',
            'phone_number' => 'required|string',
            'date_of_birth' => 'required|date',
            'user_group_id' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $updateData = $request->only([  'name',
                                        'address',
                                        'gender',
                                        'email',
                                        'status',
                                        'phone_number',
                                        'user_group_id'
                                    ]);

        $updateData += ["date_of_birth" => date('Y-m-d', strtotime($request->date_of_birth))];
        
        if ($request->hasFile('avatar')) {
            $path = $request->file('avatar')->store('public');
            $updateData += ["avatar" => substr($path, 7)];
        }       

        try {
            if(! $this->userRepository->find($id)) {
                return response()->json([
                    'error' => true,
                    'message' => 'user does not exist'
                ], 400);
            } else {
                $this->userRepository->update($id, $updateData);
            }
        }   catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
        $user = $this->userRepository->find($id);
         $user["user_group"] = $user->user_group()->pluck("name")[0];
        return response()->json(compact('user'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = $this->userRepository->find($id);

            if(! $user) {
                return response()->json([
                    'error' => true,
                    'message' => 'User does not exist'
                ], 400);
            }

            $this->userRepository->delete($id);

        }   catch (Exception $e) {
            response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('user'), 200);
    }
}

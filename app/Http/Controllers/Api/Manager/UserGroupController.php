<?php

namespace App\Http\Controllers\Api\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserGroup\UserGroupInterface;
use Validator;

class UserGroupController extends Controller
{
    protected $userGroupRepository;

    public function __construct(UserGroupInterface $userGroupRepository) {
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
    * @SWG\Get(
    *      path="/user-groups",
    *      operationId="getUserGroupsList",
    *      tags={"User Groups"},
    *      summary="Get list of user groups",
    *      description="Returns list of user groups",
    *      @SWG\Response(
    *          response=200,
    *          description="successful operation"
    *       ),
    *       @SWG\Response(response=400, description="Bad request"),
    *       security={
    *           {"api_key_security_example": {}}
    *       }
    *     )
    *
    * Returns list of user groups
    */

    public function index() {
        $userGroups = $this->userGroupRepository->all();
        foreach ($userGroups as $userGroup) {
            $wage_group = $userGroup->wage_group()->pluck("name");
            if ($wage_group->isEmpty()) {
                $userGroup['wage_group'] = null;
            } else {
                $userGroup["wage_group"] = $wage_group[0];
            }
        }

        return response()->json(compact(('userGroups')), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
    * @SWG\POST(
    *      path="/user-groups",
    *      operationId="createNewUserGroup",
    *      tags={"User Groups"},
    *      summary="Store a newly created user group in storage",
    *      description="Store a newly created user group in storage",
    *      @SWG\Parameter(
    *           name="name",
    *           in="body",
    *           description="Name of user group",
    *           required=true,
    *           schema="#/definitions/UserGroup",
    *           type="string"
    *      ),
    *      @SWG\Parameter(
    *           name="wage_groups_id",
    *           in="body",
    *           description="Target wage group",
    *           schema="#/definitions/UserGroup",
    *           required=true,
    *           type="integer"
    *      ),
    *      @SWG\Response(
    *          response=200,
    *          description="successful operation"
    *       ),
    *       @SWG\Response(response=400, description="Bad request"),
    *       security={
    *           {"api_key_security_example": {}}
    *       }
    *     )
    *
    * Store a newly created user group in storage
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'wage_group_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $insertData = $request->only(['name', 'wage_group_id']);

        try {
            $userGroup = $this->userGroupRepository->create($insertData);
            $userGroup["wage_group"] = $userGroup->wage_group()->pluck('name')[0];
        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('userGroup'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $userGroup = $this->userGroupRepository->find($id);

            if(! $userGroup) {
                return response()->json([
                    'error' => true,
                    'message' => 'user group does not exist'
                ], 400);
            } else {
                $userGroup["wage_group"] = $userGroup->wage_group()->pluck('name')[0];
            }
        }   catch (Exception $e) {
            response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('userGroup'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'wage_group_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $updateData = $request->only(['name', 'wage_group_id']);

        try {
            if(! $this->userGroupRepository->find($id)) {
                return response()->json([
                    'error' => true,
                    'message' => 'user group does not exist'
                ], 400);
            } else {
                $this->userGroupRepository->update($id, $updateData);
            }
        }   catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        $userGroup = $this->userGroupRepository->find($id);
        $userGroup["wage_group"] = $userGroup->wage_group()->pluck('name')[0];
        return response()->json(compact('userGroup'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $userGroup = $this->userGroupRepository->find($id);

            if(! $userGroup) {
                return response()->json([
                    'error' => true,
                    'message' => 'user group does not exist'
                ], 400);
            }

            $this->userGroupRepository->delete($id);

        }   catch (Exception $e) {
            response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }

        return response()->json(compact('userGroup'), 200);
    }
}
    
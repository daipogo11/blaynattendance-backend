<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserInterface;
use App\Repositories\WorkedTime\WorkedTimeInterface;
use App\Repositories\Shift\ShiftInterface;
use App\Repositories\Payroll\PayrollInterface;
use Carbon\Carbon;
use Validator;

class AttendanceController extends Controller
{
   
    protected $userRepository;
    protected $workedTimeRepository;
    protected $shiftRepository;
    protected $payrollRepository;

    public function __construct(UserInterface $userRepository,
                            WorkedTimeInterface $workedTimeRepository,
                            ShiftInterface $shiftRepository,
                            PayrollInterface $payrollRepository
    ) {
       
        $this->userRepository = $userRepository;
        
        $this->workedTimeRepository = $workedTimeRepository;
        $this->shiftRepository = $shiftRepository;
        $this->payrollRepository= $payrollRepository;
    }

    public function index() {
        try {
            $today = Carbon::now();
            $year=$today->year;
            $month=$today->month;
          
            $workedTimes =  auth()->user()->worked_times()->whereYear('in_time',$year)->whereMonth('in_time',$month)->get();
            foreach ($workedTimes as $item) {
                $item['shift_name'] = $item->shift->shift_pattern()->pluck('name')[0];
                $item['shift_day']=$item->shift()->pluck('date')[0];
                // $item['year']= $item->shift()->pluck('date')[0]->year;
               
            }
            return response()->json(compact('workedTimes'), 200);

        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function salary(){
        try {
          
            $salary =  auth()->user()->payrolls() ->orderBy('date', 'desc')->get();
            foreach ($salary as $item) {
               
              $item['year']=substr((string)$item['date'],0,4);
              $item['month']=substr((string)$item['date'],5,2);
              $item['time']=$item['month'].'-'.$item['year'];
            }
            //lấy ra các tháng trong mảng
            $payroll=[];
            $x=0;
            foreach($salary as $item){
                $payroll[$x]=$item['time'];
                $x++;
            }
            //xử lý để loại bỏ trùng lặp
            $result=array_unique($payroll);
            $kq=[];
            $y=0;
            foreach($result as $item){
                $kq[$y]['time']=$item;
                $amount=0;
                foreach($salary as $i){
                    if($item==$i['time']){
                        $amount=$amount+$i['amount'];
                    }
                }
                $kq[$y]['amount']=$amount;
                $amount=0;
                $y++;
            }
            return response()->json(compact('kq'), 200);

        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function allSalary() {
        try {
           
            $salary =  auth()->user()->payrolls()->get();
            foreach ($salary as $item) {
               
              $item['year']=substr((string)$item['date'],0,4);
              $item['month']=substr((string)$item['date'],5,2);
              $item['time']=$item['month'].'-'.$item['year'];
            }
            return response()->json(compact('salary'), 200);

        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
    public function allAttendance() {
        try {
           
            $allAttendance =  auth()->user()->worked_times()->get();
            foreach ($allAttendance as $item) {
                $item['shift_name'] = $item->shift->shift_pattern()->pluck('name')[0];
                $item['shift_day']=$item->shift()->pluck('date')[0];
                $item['time']=substr((string)$item['shift_day'],5,2).'-'.substr((string)$item['shift_day'],0,4);
               
            }
            return response()->json(compact('allAttendance'), 200);

        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }

}

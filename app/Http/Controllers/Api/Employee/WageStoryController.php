<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\WageGroup\WageGroupInterface;
use App\Repositories\Wage\WageInterface;
use App\Repositories\User\UserInterface;
use App\Repositories\UserGroup\UserGroupInterface;
use Validator;

class WageStoryController extends Controller
{
    protected $wageGroupRepository;
    protected $wageRepository;
    protected $userRepository;
    protected $userGroupRepository;

    public function __construct(WageGroupInterface $wageGroupRepository, WageInterface $wageRepository,
    UserInterface $userRepository, UserGroupInterface $userGroupRepository) {
        $this->wageGroupRepository = $wageGroupRepository;
        $this->wageRepository = $wageRepository;
        $this->userRepository = $userRepository;
        $this->userGroupRepository = $userGroupRepository;
    }

    public function getStory() {
        try {
            $wageStory =  auth()->user()->user_group->wage_group->wages()
                            ->orderBy('start_date', 'desc')->orderBy('created_at', 'desc')->get();
            foreach ($wageStory as $item) {
                $item['wage_group'] = $item->wage_group()->pluck('name')[0];
            }

            return response()->json(compact('wageStory'), 200);

        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}

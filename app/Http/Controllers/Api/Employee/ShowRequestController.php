<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Repositories\PendingPayrollRequest\PendingPayrollRequestInterface;
use App\Repositories\PendingShiftRequest\PendingShiftRequestInterface;
use App\Repositories\FinishedPayrollRequest\FinishedPayrollRequestInterface;
use App\Repositories\FinishedShiftRequest\FinishedShiftRequestInterface;
use App\Repositories\User\UserInterface;
use App\Repositories\Shift\ShiftInterface;
use App\Repositories\ShiftMaster\ShiftMasterInterface;
use App\Repositories\ShiftPattern\ShiftPatternInterface;

class  ShowRequestController extends Controller
{
    protected $pendingPayrollRequestRepository;
    protected $finishedPayrollRequestRepository;
    protected $pendingShiftRequestRepository;
    protected $finishedShiftRequestRepository;
    protected $userRepository;
    protected $shiftRepository;
    protected $shiftPatternRepository;

    public function __construct(PendingPayrollRequestInterface $pendingPayrollRequestRepository,
    							PendingShiftRequestInterface $pendingShiftRequestRepository,
    							FinishedPayrollRequestInterface $finishedPayrollRequestRepository,
                                FinishedShiftRequestInterface $finishedShiftRequestRepository,
                                UserInterface $userRepository,
                                ShiftInterface $shiftRepository,
                                ShiftPatternInterface $shiftPatternRepository)
    {
    	$this->pendingPayrollRequestRepository = $pendingPayrollRequestRepository;
    	$this->pendingShiftRequestRepository = $pendingShiftRequestRepository;
    	$this->finishedPayrollRequestRepository = $finishedPayrollRequestRepository;
        $this->finishedShiftRequestRepository = $finishedShiftRequestRepository;
        $this->userRepository = $userRepository;
        $this->shiftRepository = $shiftRepository;
        $this->shiftPatternRepository = $shiftPatternRepository;
    }

    /**show dơn xin nghi ca
    */
    public function getPendingLeaveShiftRequest() {
        try {
            $pendingLeaveShift =  auth()->user()->pending_shift_requests()->where('request_type_id',2)
            ->orderBy('created_at', 'desc')->get();

            $finishedLeaveShift =  auth()->user()->finished_shift_requests()->where('request_type_id',2)
            ->orderBy('created_at', 'desc')->get();

            foreach ($pendingLeaveShift as $item) {
                $item['status'] = null;
                $item['pheduyet']='Chờ phê duyệt';
                $item['date'] =  $this->shiftRepository->find($item['old_shift_id'])->pluck('date')[$item['old_shift_id']-1];
                $item['name']=$this->shiftRepository->find($item['old_shift_id'])->shift_pattern()->pluck('name')[0];
                $item['han_phe_duyet']= $item['created_at']->addDays(2)->toDateTimeString(); 
            }
            foreach ($finishedLeaveShift as $item) {
                $item['pheduyet']='';
                if($item['status']=1){
                    $item['pheduyet']='Chấp nhận';
                }else{
                    $item['pheduyet']='Không chấp nhận';
                }
                $item['date'] =  $this->shiftRepository->find($item['old_shift_id'])->pluck('date')[$item['old_shift_id']-1];
                $item['name']=$this->shiftRepository->find($item['old_shift_id'])->shift_pattern()->pluck('name')[0];
                $item['han_phe_duyet']= $item['created_at']->addDays(2)->toDateTimeString(); 
            }
            $pendingLeaveShift = $pendingLeaveShift->values()->all();
            $leaveShiftRequest = collect($pendingLeaveShift)->merge($finishedLeaveShift)->sortByDesc('created_at');
            return response()->json(compact('leaveShiftRequest'), 200);
        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
   
    //show đơn xin chuyển ca
    public function getChangeShiftRequest() {
        try {
            $pendingLeaveShift =  auth()->user()->pending_shift_requests()->where('request_type_id',1)
            ->orderBy('created_at', 'desc')->get();

            $finishedLeaveShift =  auth()->user()->finished_shift_requests()->where('request_type_id',1)
            ->orderBy('created_at', 'desc')->get();

            foreach ($pendingLeaveShift as $item) {
                $item['status'] = null;
                $item['pheduyet']='Chờ phê duyệt';
                $item['date'] =  $this->shiftRepository->find($item['old_shift_id'])->pluck('date')[$item['old_shift_id']-1];
                $item['name_old']=$this->shiftRepository->find($item['old_shift_id'])->shift_pattern()->pluck('name')[0];
                $item['name_change']=$this->shiftRepository->find($item['shift_change_id'])->shift_pattern()->pluck('name')[0];
                $item['han_phe_duyet']= $item['created_at']->addDays(2)->toDateTimeString(); 
                
            }
            foreach ($finishedLeaveShift as $item) {
                $item['pheduyet']='';
                if($item['status']=1){
                    $item['pheduyet']='Chấp nhận';
                }else{
                    $item['pheduyet']='Không chấp nhận';
                }
                $item['date'] =  $this->shiftRepository->find($item['old_shift_id'])->pluck('date')[$item['old_shift_id']-1];
                $item['name_old']=$this->shiftRepository->find($item['old_shift_id'])->shift_pattern()->pluck('name')[0];
                $item['name_change']=$this->shiftRepository->find($item['shift_change_id'])->shift_pattern()->pluck('name')[0];
                $item['han_phe_duyet']= $item['created_at']->addDays(2)->toDateTimeString(); 
               
            }
            $pendingLeaveShift = $pendingLeaveShift->values()->all();
            $leaveShiftRequest = collect($pendingLeaveShift)->merge($finishedLeaveShift)->sortByDesc('created_at');
            return response()->json(compact('leaveShiftRequest'), 200);

        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
    //show đơnnghỉ viẹc
    public function getLeaveRequest() {
        try {
            $pendingLeave =  auth()->user()->pending_leave_requests()
            ->orderBy('created_at', 'desc')->get();

            $finishedLeave =  auth()->user()->finished_leave_requests()
            ->orderBy('created_at', 'desc')->get();

            foreach ($pendingLeave as $item) {
                $item['status'] = null;
                $item['pheduyet']='Chờ phê duyệt';
                $item['han_phe_duyet']= $item['created_at']->addDays(10)->toDateTimeString(); 
            }
            foreach ($finishedLeave as $item) {
                $item['han_phe_duyet']= $item['created_at']->addDays(10)->toDateTimeString(); 
                $item['pheduyet']='';
                if($item['status']=1){
                    $item['pheduyet']='Chấp nhận';
                }else{
                    $item['pheduyet']='Không chấp nhận';
                }
               }
            $pendingLeave = $pendingLeave->values()->all();
            $leaveRequest = collect($pendingLeave)->merge($finishedLeave)->sortByDesc('created_at');
            return response()->json(compact('leaveRequest'), 200);

        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
    //show đơn xin ứng lương
    public function getPayrollRequest() {
        try {
            $pendingLeave =  auth()->user()->pending_payroll_requests()
            ->orderBy('created_at', 'desc')->get();

            $finishedLeave =  auth()->user()->finished_payroll_requests()
            ->orderBy('created_at', 'desc')->get();

            foreach ($pendingLeave as $item) {
                $item['status'] = null;
               $item['pheduyet']='Chờ phê duyệt';
               $item['han_phe_duyet']= $item['created_at']->addDays(3)->toDateTimeString(); 
            }
            foreach ($finishedLeave as $item) {
                $item['han_phe_duyet']= $item['created_at']->addDays(3)->toDateTimeString(); 
                $item['pheduyet']='';
                if($item['status']=1){
                    $item['pheduyet']='Chấp nhận';
                }else{
                    $item['pheduyet']='Không chấp nhận';
                }
            }
            $pendingLeave = $pendingLeave->values()->all();
            $leaveRequest = collect($pendingLeave)->merge($finishedLeave)->sortByDesc('created_at');
            return response()->json(compact('leaveRequest'), 200);

        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }

  
}

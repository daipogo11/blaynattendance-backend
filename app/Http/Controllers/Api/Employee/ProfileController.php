<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Repositories\User\UserInterface;



class ProfileController extends Controller
{
    protected $userRepository;
    

    public function __construct(UserInterface $userRepository)
    {
    	$this->userRepository = $userRepository;
    }

    /**
    */
    public function updateProfile(Request $request) {
        $validator = Validator::make($request->all(), [
            'address' => 'required|string',
            'email' => 'required|string|unique:users,email,' . auth()->user()->id,
            'phone_number' => 'required|string'
        ]);

    	if ($validator->fails()) {
    		return response()->json($validator->errors());
    	}
        $updateData = $request->only([
            'address',
            'email',
            'phone_number'
        ]);
       
        try {
            $id = auth()->user()->id;
            $this->userRepository->update($id,$updateData);
        }   catch (Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
        $user =  $this->userRepository->find($id);
        return response()->json(compact('user'), 200);
    }
    
   /**
    */
    public function getProfile(){
        try {
            // $id = auth()->user()->id;
            $profile =  $this->userRepository->get();
           
            return response()->json(compact('profile'), 200);

        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}

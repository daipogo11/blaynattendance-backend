<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Repositories\PendingPayrollRequest\PendingPayrollRequestInterface;
use App\Repositories\PendingShiftRequest\PendingShiftRequestInterface;
use App\Repositories\FinishedPayrollRequest\FinishedPayrollRequestInterface;
use App\Repositories\FinishedShiftRequest\FinishedShiftRequestInterface;

class RequestController extends Controller
{
    protected $pendingPayrollRequestRepository;
    protected $finishedPayrollRequestRepository;
    protected $pendingShiftRequestRepository;
    protected $finishedShiftRequestRepository;

    public function __construct(PendingPayrollRequestInterface $pendingPayrollRequestRepository,
    							PendingShiftRequestInterface $pendingShiftRequestRepository,
    							FinishedPayrollRequestInterface $finishedPayrollRequestRepository,
    							FinishedShiftRequestInterface $finishedShiftRequestRepository)
    {
    	$this->pendingPayrollRequestRepository = $pendingPayrollRequestRepository;
    	$this->pendingShiftRequestRepository = $pendingShiftRequestRepository;
    	$this->finishedPayrollRequestRepository = $finishedPayrollRequestRepository;
    	$this->finishedShiftRequestRepository = $finishedShiftRequestRepository;
    }

    /**
    */
    public function submitPayrollRequest(Request $request) {
    	$validator = Validator::make($request->all(), [
    		'time' => 'required|date',
    		'amount' => 'required'
    	]);

    	if ($validator->fails()) {
    		return response()->json($validator->errors());
    	}

    	$insertData = $request->only(['amount', 'description']);
    	$insertData['time'] = date('Y-m-d', strtotime($request->time));
    	$insertData['user_id'] = auth()->user()->id;

    	try {
    		$pendingPayrollRequest = $this->pendingPayrollRequestRepository->create($insertData);
    		return response()->json(compact('pendingPayrollRequest'), 200);
    	} catch (Exception $e) {
    		return response()->json($e->getMessage(), 500);
    	}
    }

    public function submitChangeShiftRequest(Request $request) {
        $validator = Validator::make($request->all(), [
            'old_shift_id' => 'required|integer',
            'shift_change_id' => 'required|integer'

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $insertData = $request->only(['old_shift_id', 'shift_change_id', 'description']);
        $insertData['employee_id'] = auth()->user()->id;
        $insertData['request_type_id'] = 1;

        try {
            $pendingShiftRequest = $this->pendingShiftRequestRepository->create($insertData);
            return response()->json(compact(('pendingShiftRequest')), 200);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function submitLeaveShiftRequest(Request $request) {
        $validator = Validator::make($request->all(), [
            'old_shift_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $insertData = $request->only(['old_shift_id', 'description']);
        $insertData['shift_change_id'] = null;
        $insertData['employee_id'] = auth()->user()->id;
        $insertData['request_type_id'] = 2;

        try {
            $pendingShiftRequest = $this->pendingShiftRequestRepository->create($insertData);
            return response()->json(compact(('pendingShiftRequest')), 200);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}

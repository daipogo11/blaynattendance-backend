<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Repositories\PendingLeaveRequest\PendingLeaveRequestInterface;

use App\Repositories\FinishedLeaveRequest\FinishedLeaveRequestInterface;


class LeaveRequestController extends Controller
{
    protected $pendingLeaveRequestRepository;
    protected $finishedLeaveRequestRepository;
    

    public function __construct(PendingLeaveRequestInterface $pendingLeaveRequestRepository,
    							
    							FinishedLeaveRequestInterface $finishedLeaveRequestRepository
    							)
    {
    	$this->pendingLeaveRequestRepository = $pendingLeaveRequestRepository;
    	$this->finishedLeaveRequestRepository = $finishedLeaveRequestRepository;
    
    }

    /**
    */
    public function submitLeaveRequest(Request $request) {
    	$validator = Validator::make($request->all(), [
    		'day' => 'required|date',
    		'description' => 'required'
    	]);

    	if ($validator->fails()) {
    		return response()->json($validator->errors());
    	}

        $insertData = $request->only(['description']);
        $insertData['day'] = date('Y-m-d', strtotime($request->day));
    	$insertData['user_id'] = auth()->user()->id;

    	try {
    		$pendingLeaveRequest = $this->pendingLeaveRequestRepository->create($insertData);
    		return response()->json(compact('pendingLeaveRequest'), 200);
    	} catch (Exception $e) {
    		return response()->json($e->getMessage(), 500);
    	}
    }
   
}

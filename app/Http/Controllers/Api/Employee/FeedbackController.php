<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Repositories\Feedback\FeedbackInterface;


class FeedbackController extends Controller
{
    protected $FeedbackRepository;
   

    public function __construct(FeedbackInterface $feedbackRepository
    							)
    {
    	$this->feedbackRepository = $feedbackRepository;
    }

    /**
    */
    public function submitFeedbackRequest(Request $request) {
    	$validator = Validator::make($request->all(), [
    		'title' => 'required',
    		'content' => 'required'
    	]);

    	if ($validator->fails()) {
    		return response()->json($validator->errors());
    	}

    	$insertData = $request->only(['title', 'content']);
    	$insertData['send_id'] = auth()->user()->id;

    	try {
            $feedbackRequest = $this->feedbackRepository->create($insertData);
    		return response()->json(compact(('feedbackRequest')), 200);
    	} catch (Exception $e) {
    		return response()->json($e->getMessage(), 500);
    	}
    }

}

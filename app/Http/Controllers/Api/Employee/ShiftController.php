<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Shift\ShiftInterface;
use App\Repositories\ShiftMaster\ShiftMasterInterface;
use App\Repositories\ShiftPattern\ShiftPatternInterface;
use App\Models\User;

class ShiftController extends Controller
{
    protected $shiftRepository;
    protected $shiftPatternRepository;

    public function __construct(ShiftInterface $shiftRepository,
      ShiftPatternInterface $shiftPatternRepository) {
        $this->shiftRepository = $shiftRepository;
        $this->shiftPatternRepository = $shiftPatternRepository;
	}

    public function getShifts() {
    	$shifts = auth()->user()->shifts()->get();

    	foreach ($shifts as $shift) {
            $shift_pattern = $shift->shift_pattern();
            $shift['start_time'] = $shift_pattern->pluck('start_time')[0];
            $shift['end_time'] = $shift_pattern->pluck('end_time')[0];
            $shift['name'] = $shift_pattern->pluck('name')[0];
        }
    	return response()->json(compact('shifts'), 200);
    }

    public function detailShift($id) {
        $shift = $this->shiftRepository->find($id);
        $shift['name'] = $shift->shift_pattern()->pluck('name')[0];
        return response()->json(compact('shift'), 200);
    }

    public function shiftInDay($id) {
        $shifts = auth()->user()->shifts()->get();
        $idShifts=[];
        
        $shiftsOfEmployee = auth()->user()->shifts()->get();
        $shift =$this->shiftRepository->find($id);
        $date= $shift->pluck('date')[0];
        $shiftInDay=$this->shiftRepository->whereDate('date',$date)->where('id','<>',$id)->get();
        
        foreach ($shiftInDay as $shiftInDay) {
            $shift_pattern = $shiftInDay->shift_pattern();
            $shiftInDay['name'] = $shift_pattern->pluck('name')[0];
        }
        return response()->json(compact('shiftInDay'), 200);
    }

    public function shiftsInStore() {

        $shifts = auth()->user()->shifts()->get();
        $idShifts=[];
        $i=0;
        foreach ($shifts as $item) {
          $idShifts[$i]=$item['id'];
          $i++;
        }
        
        $shiftsInStore =$this->shiftRepository->whereNotIn('id',$idShifts)->get();
        
        foreach ($shiftsInStore as $item) {
            $item['name'] = $item->shift_pattern()->pluck('name')[0];
         
        }
       
        return response()->json(compact('shiftsInStore'), 200);
    }
}

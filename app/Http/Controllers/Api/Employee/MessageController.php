<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Message\MessageInterface;
use App\Repositories\User\UserInterface;
use Validator;

class MessageController extends Controller
{
    protected $messageRepository;
    protected $userRepository;
   

    public function __construct(
        UserInterface $userRepository,
        MessageInterface $messageRepository      
    ){
        $this->userRepository = $userRepository;
        $this->messageRepository = $messageRepository;
    }

    
    
    public function postMessage(Request $request){
        $validator = Validator::make($request->all(), [
            'content'=>'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $insertData = $request->only(['content']);
        $insertData['receive_id'] =1;
        $insertData['user_id'] = auth()->user()->id;
        try {
            $messageRequest = $this->messageRepository->create($insertData);
            return response()->json(compact(('messageRequest')), 200);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}

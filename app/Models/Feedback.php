<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = "feedbacks";
    protected $fillable = ['title', 'content', 'send_id'];

    public function user() {
        return $this->belongsTo('App\Models\User', 'send_id');
    }
}

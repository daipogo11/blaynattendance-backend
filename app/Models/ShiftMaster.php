<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShiftMaster extends Model
{
    protected $table = "shift_master";

    protected $fillable = ['start_date', 'end_date', 'description', 'shift_pattern_id'];

    public function shift_pattern() {
        return $this->belongsTo('App\Models\ShiftPattern', 'shift_pattern_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PendingLeaveRequest extends Model
{
    protected $table = "pending_leave_request";
    protected $fillable = ['day','description', 'user_id'];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}

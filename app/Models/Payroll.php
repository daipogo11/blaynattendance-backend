<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    protected $table = "payrolls";
    protected $fillable = ["date","amount","user_id"];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}

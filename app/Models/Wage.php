<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wage extends Model
{
    protected $table = "wage";
    protected $fillable = ["salary", "start_date", "wage_group_id"];

    public function wage_group() {
        return $this->belongsTo('App\Models\WageGroup', 'wage_group_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Wage;

class WageGroup extends Model
{
    protected $table = "wage_groups";

    protected $fillable = ['name'];

    public function wages() {
        return $this->hasMany('App\Models\Wage');
    }

    public function user_groups() {
        return $this->hasMany('App\Models\UserGroup');
    }

    public function getCurrentWage() {
    	$today = Carbon::today()->format('Y-m-d');
    	
    	$wage = Wage::where('wage_group_id', $this->id)->whereDate('start_date', $today)->first();

    	if ($wage) {
            return $wage;
        } else {
    		$wage =  Wage::where('wage_group_id', $this->id)->whereDate('start_date', '<', $today)->orderBy('start_date', 'desc')->first();

    		return $wage ? $wage : null;
    	}


    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinishedLeaveRequest extends Model
{
    protected $table = "finished_leave_request";
    protected $fillable = ['day', 'description', 'user_id','status'];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}

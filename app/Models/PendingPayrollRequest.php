<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PendingPayrollRequest extends Model
{
    protected $table = "pending_payroll_request";
    protected $fillable = ['time', 'amount', 'description', 'user_id'];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}

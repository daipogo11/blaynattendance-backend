<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinishedPayrollRequest extends Model
{
    protected $table = "finished_payroll_request";
    protected $fillable = ['time', 'amount', 'description', 'user_id','status'];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}

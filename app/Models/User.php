<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
//use \Illuminate\Contracts\Auth\Authenticatable;
//use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Role;

class User extends Authenticatable implements JWTSubject
{

    protected $table = "users";

    protected $fillable = [ 'name',
                            'password',
                            'address',
                            'gender',
                            'email',
                            'avatar',
                            'status',
                            'phone_number',
                            'date_of_birth',
                            'user_group_id'
                        ];

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\Role', 'user_has_role');
    }

    public function shifts(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\Shift', 'employee_shift', 'employee_id', 'shift_id');
    }

    public function user_group() {
        return $this->belongsTo('App\Models\UserGroup', 'user_group_id');
    }

    public function pending_payroll_requests() {
        return $this->hasMany('App\Models\PendingPayrollRequest','user_id');
    }

    public function finished_payroll_requests() {
        return $this->hasMany('App\Models\FinishedPayrollRequest','user_id');
    }

    public function pending_shift_requests() {
        return $this->hasMany('App\Models\PendingShiftRequest','employee_id');
    }

    public function finished_shift_requests() {
        return $this->hasMany('App\Models\FinishedShiftRequest','employee_id');
    }

    public function payrolls() {
        return $this->hasMany('App\Models\Payroll','user_id');
    }

    public function exception_shifts() {
        return $this->hasMany('App\Models\ExceptionShift');
    }

    public function worked_times() {
        return $this->hasMany('App\Models\WorkedTime','employee_id');
    }

    public function notifications() {
        return $this->hasMany('App\Models\Notification','sent_id');
    }

    public function getRoleNames()
    {
        return $this->roles->pluck('name');
    }
    public function feedbacks() {
        return $this->hasMany('App\Models\Feedback');
    }
    public function pending_leave_requests() {
        return $this->hasMany('App\Models\PendingLeaveRequest','user_id');
    }

    public function finished_leave_requests() {
        return $this->hasMany('App\Models\FinishedLeaveRequest','user_id');
    }

    //can remove
    public function hasRole($role)
    {
        if (is_string($role) && !$role = Role::findRole($role)) {
            return false;
        }

        return $this->roles->contains('id', $role->id);
    }

    public function hasAnyRoles($names)
    {
        if (is_string($names)) {
            if (false !== strpos($names, '|')) {
                $names = $this->convertPipeToArray($names);
            } else {
                $names = [$names];
            }

        }

        return $this->roles->whereIn('name', $names)->isNotEmpty();
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    protected function convertPipeToArray(string $pipeString)
    {
        $pipeString = trim($pipeString);

        if (strlen($pipeString) <= 2) {
            return $pipeString;
        }

        $quoteCharacter = substr($pipeString, 0, 1);
        $endCharacter = substr($quoteCharacter, -1, 1);

        if ($quoteCharacter !== $endCharacter) {
            return explode('|', $pipeString);
        }

        if (!in_array($quoteCharacter, ["'", '"'])) {
            return explode('|', $pipeString);
        }

        return explode('|', trim($pipeString, $quoteCharacter));
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinishedShiftRequest extends Model
{
    protected $table = "finished_shift_request";

    public function employee() {
        return $this->belongsTo('App\Models\User', 'employee_id');
    }

    public function shift() {
        return $this->belongsTo('App\Models\Shift', 'shift_id');
    }
}

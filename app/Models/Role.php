<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    protected $table = "roles";

    public function users(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\User','user_has_role');
    }

    public static function findRole($name)
    {
        return static::where('name', $name)->first();
    }
}

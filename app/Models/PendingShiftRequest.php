<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PendingShiftRequest extends Model
{
    protected $table = "pending_shift_request";

    protected $fillable = ['employee_id', 'old_shift_id', 'shift_change_id', 'description', 'request_type_id'];

    public function employee() {
        return $this->belongsTo('App\Models\User', 'employee_id');
    }

    public function shift() {
        return $this->belongsTo('App\Models\Shift', 'shift_id');
    }
}

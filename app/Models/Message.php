<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "messages";
    protected $fillable = ['content', 'user_id','receive_id'];

    public function receive() {
        return $this->belongsTo('App\Models\User', 'receive_id');
    }
    public function send() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    // public function comment(){
    //     return $this->hasMany('App\Models\Comment','notification_id');
    // }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ShiftPattern extends Model
{
    protected $table = "shift_pattern";

    protected $fillable = ['start_time', 'end_time', 'description', 'factor_salary', 'name'];

    public function shift_masters() {
        return $this->hasMany('App\Models\ShiftMaster');
    }

    public function shifts() {
        return $this->hasMany('App\Models\Shift');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExceptionShift extends Model
{
    protected $table = "exception_shift";

    public function employee() {
        return $this->belongsTo('App\Models\User', 'employee_id');
    }

    public function shift() {
        return $this->belongsTo('App\Models\Shift', 'shift_id');
    }
}

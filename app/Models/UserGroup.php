<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table = "user_groups";

    protected $fillable = ['name', 'wage_group_id'];

    public function users() {
        return $this->hasMany('App\Models\User');
    }

    public function wage_group() {
        return $this->belongsTo('App\Models\WageGroup', 'wage_group_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkedTime extends Model
{
    protected $table = "worked_time";

    protected $fillable = ["in_time", "out_time", 'employee_id', 'shift_id'];

    public $timestamps = false;

    public function employee() {
        return $this->belongsTo('App\Models\User', 'employee_id');
    }

    public function shift() {
        return $this->belongsTo('App\Models\Shift', 'shift_id');
    }
}

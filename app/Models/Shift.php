<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Shift extends Model
{
    protected $table = "shift";

    protected $fillable = ['date', 'shift_pattern_id', 'shift_master_id'];
    public $timestamps = false;

    public function shift_pattern() {
        return $this->belongsTo('App\Models\ShiftPattern', 'shift_pattern_id');
    }

    public function exception_shifts() {
        return $this->hasMany('App\Models\ExceptionShift');
    }

    public function pending_shift_requests() {
        return $this->hasMany('App\Models\PendingShiftRequest');
    }

    public function finished_shift_requests() {
        return $this->hasMany('App\Models\FinishedShiftRequest');
    }

    public function employees(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\User', 'employee_shift', 'shift_id', 'employee_id');
    }

    public function workedTimes()
    {
        return $this->hasMany('App\Models\WorkedTime', 'shift_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = "notifications";
    protected $fillable = ['title', 'content', 'sent_id'];

    public function receive() {
        return $this->belongsTo('App\Models\User', 'sent_id');
    }
    public function comment(){
        return $this->hasMany('App\Models\Comment','notification_id');
    }

}

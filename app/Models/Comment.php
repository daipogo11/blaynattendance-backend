<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";
    protected $fillable = [ 'content', 'send_id', 'notification_id'];

    public function receive() {
        return $this->belongsTo('App\Models\User', 'send_id');
    }
    public function notification(){
        return $this->belongTo('App\Models\Notification', 'notification_id');
    }
}

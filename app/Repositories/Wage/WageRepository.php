<?php

namespace App\Repositories\Wage;

use App\Models\Wage;
use App\Repositories\BaseRepository;

class WageRepository extends BaseRepository implements WageInterface
{
    public function __construct(Wage $wage)
    {
        parent::__construct($wage);
    }
}

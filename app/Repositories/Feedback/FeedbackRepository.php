<?php

namespace App\Repositories\Feedback;

use App\Models\Feedback;
use App\Repositories\BaseRepository;

class FeedbackRepository extends BaseRepository implements FeedbackInterface
{
    public function __construct(Feedback $feedback)
    {
        parent::__construct($feedback);
    }
}

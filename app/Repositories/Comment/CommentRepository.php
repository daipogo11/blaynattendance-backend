<?php

namespace App\Repositories\Comment;

use App\Models\Comment;
use App\Repositories\BaseRepository;

class CommentRepository extends BaseRepository implements CommentInterface
{
    public function __construct(Comment $comment)
    {
        parent::__construct($comment);
    }
}

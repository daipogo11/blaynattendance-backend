<?php

namespace App\Repositories\WageGroup;

use App\Models\WageGroup;
use App\Repositories\BaseRepository;

class WageGroupRepository extends BaseRepository implements WageGroupInterface
{
    public function __construct(WageGroup $wage_group)
    {
        parent::__construct($wage_group);
    }
}

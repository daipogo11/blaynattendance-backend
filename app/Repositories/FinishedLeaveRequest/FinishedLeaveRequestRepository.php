<?php

namespace App\Repositories\FinishedLeaveRequest;

use App\Models\FinishedLeaveRequest;
use App\Repositories\BaseRepository;

class FinishedLeaveRequestRepository extends BaseRepository implements FinishedLeaveRequestInterface
{
    public function __construct(FinishedLeaveRequest $finished_leave_request)
    {
        parent::__construct($finished_leave_request);
    }
}

<?php

namespace App\Repositories\PendingLeaveRequest;

use App\Models\PendingLeaveRequest;
use App\Repositories\BaseRepository;

class PendingLeaveRequestRepository extends BaseRepository implements PendingLeaveRequestInterface
{
    public function __construct(PendingLeaveRequest $pending_leave_request)
    {
        parent::__construct($pending_leave_request);
    }
}

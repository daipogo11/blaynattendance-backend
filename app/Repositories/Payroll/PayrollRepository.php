<?php

namespace App\Repositories\Payroll;

use App\Models\Payroll;
use App\Repositories\BaseRepository;

class PayrollRepository extends BaseRepository implements PayrollInterface
{
    public function __construct(Payroll $payroll)
    {
        parent::__construct($payroll);
    }
}

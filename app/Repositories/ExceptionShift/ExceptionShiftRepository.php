<?php

namespace App\Repositories\ExceptionShift;

use App\Models\ExceptionShift;
use App\Repositories\BaseRepository;

class ExceptionShiftRepository extends BaseRepository implements ExceptionShiftInterface
{
    public function __construct(ExceptionShift $exception_shift)
    {
        parent::__construct($exception_shift);
    }
}

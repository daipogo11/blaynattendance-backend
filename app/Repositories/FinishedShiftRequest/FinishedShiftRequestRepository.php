<?php

namespace App\Repositories\FinishedShiftRequest;

use App\Models\FinishedShiftRequest;
use App\Repositories\BaseRepository;

class FinishedShiftRequestRepository extends BaseRepository implements FinishedShiftRequestInterface
{
    public function __construct(FinishedShiftRequest $finished_shift_request)
    {
        parent::__construct($finished_shift_request);
    }
}

<?php

namespace App\Repositories\Shift;

use App\Models\Shift;
use App\Repositories\BaseRepository;

class ShiftRepository extends BaseRepository implements ShiftInterface
{
    public function __construct(Shift $shift)
    {
        parent::__construct($shift);
    }
}

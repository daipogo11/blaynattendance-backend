<?php

namespace App\Repositories\WorkedTime;

use App\Models\WorkedTime;
use App\Repositories\BaseRepository;

class WorkedTimeRepository extends BaseRepository implements WorkedTimeInterface
{
    public function __construct(WorkedTime $worked_time)
    {
        parent::__construct($worked_time);
    }
}

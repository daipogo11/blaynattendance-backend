<?php

namespace App\Repositories\PendingPayrollRequest;

use App\Models\PendingPayrollRequest;
use App\Repositories\BaseRepository;

class PendingPayrollRequestRepository extends BaseRepository implements PendingPayrollRequestInterface
{
    public function __construct(PendingPayrollRequest $pending_payroll_request)
    {
        parent::__construct($pending_payroll_request);
    }
}

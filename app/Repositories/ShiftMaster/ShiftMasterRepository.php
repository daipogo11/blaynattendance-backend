<?php

namespace App\Repositories\ShiftMaster;

use App\Models\ShiftMaster;
use App\Repositories\BaseRepository;

class ShiftMasterRepository extends BaseRepository implements ShiftMasterInterface
{
    public function __construct(ShiftMaster $shift_master)
    {
        parent::__construct($shift_master);
    }
}

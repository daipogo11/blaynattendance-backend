<?php

namespace App\Repositories\ShiftPattern;

use App\Models\ShiftPattern;
use App\Repositories\BaseRepository;

class ShiftPatternRepository extends BaseRepository implements ShiftPatternInterface
{
    public function __construct(ShiftPattern $ShiftPattern)
    {
        parent::__construct($ShiftPattern);
    }
}

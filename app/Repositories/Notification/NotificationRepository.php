<?php

namespace App\Repositories\Notification;

use App\Models\Notification;
use App\Repositories\BaseRepository;

class NotificationRepository extends BaseRepository implements NotificationInterface
{
    public function __construct(Notification $notification)
    {
        parent::__construct($notification);
    }
}

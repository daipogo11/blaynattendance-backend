<?php

namespace App\Repositories\UserGroup;

use App\Models\UserGroup;
use App\Repositories\BaseRepository;

class UserGroupRepository extends BaseRepository implements UserGroupInterface
{
    public function __construct(UserGroup $user_group)
    {
        parent::__construct($user_group);
    }
}

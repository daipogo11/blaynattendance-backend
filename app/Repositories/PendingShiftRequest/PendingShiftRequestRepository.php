<?php

namespace App\Repositories\PendingShiftRequest;

use App\Models\PendingShiftRequest;
use App\Repositories\BaseRepository;

class PendingShiftRequestRepository extends BaseRepository implements PendingShiftRequestInterface
{
    public function __construct(PendingShiftRequest $pending_shift_request)
    {
        parent::__construct($pending_shift_request);
    }
}

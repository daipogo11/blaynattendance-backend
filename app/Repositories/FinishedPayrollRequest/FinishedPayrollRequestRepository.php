<?php

namespace App\Repositories\FinishedPayrollRequest;

use App\Models\FinishedPayrollRequest;
use App\Repositories\BaseRepository;

class FinishedPayrollRequestRepository extends BaseRepository implements FinishedPayrollRequestInterface
{
    public function __construct(FinishedPayrollRequest $finished_payroll_request)
    {
        parent::__construct($finished_payroll_request);
    }
}

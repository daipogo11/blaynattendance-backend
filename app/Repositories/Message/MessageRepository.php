<?php

namespace App\Repositories\Message;

use App\Models\Message;
use App\Repositories\BaseRepository;

class MessageRepository extends BaseRepository implements MessageInterface
{
    public function __construct(Message $message)
    {
        parent::__construct($message);
    }
}

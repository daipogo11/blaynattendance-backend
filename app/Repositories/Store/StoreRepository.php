<?php

namespace App\Repositories\Store;

use App\Models\Store;
use App\Repositories\BaseRepository;

class StoreRepository extends BaseRepository implements StoreInterface
{
    public function __construct(Store $store)
    {
        parent::__construct($store);
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['jwt.auth']], function() {
	Route::get('current-user', 'AuthController@getCurrentUser');
    Route::post('logout', 'AuthController@logout');

    Route::group(['namespace' => 'Manager', 'prefix' => 'manager', 'middleware' => 'admin'], function() {
        Route::resource('wage-groups', 'WageGroupController')->except(['create', 'edit']);
        Route::get('wage-story', 'WageGroupController@getStory');

	    Route::resource('user-groups', 'UserGroupController')->except(['create', 'edit']);
	    Route::resource('users', 'UserController')->except(['create', 'edit']);
        Route::resource('shift-patterns', 'ShiftPatternController')->except(['create', 'edit']);

        Route::resource('shift-masters', 'ShiftController')->except(['create', 'edit']);
        Route::get('shifts', 'ShiftController@getShifts');
        Route::get('shifts/{id}', 'ShiftController@showShift');
        Route::post('schedule-shift', 'ShiftController@scheduleShift');

	    Route::post('check-in', 'AttendanceController@check_in');
	    Route::post('check-out', 'AttendanceController@check_out');
        Route::get('attendance', 'AttendanceController@index');


        Route::get('payroll', 'PayrollController@index');
        Route::get('payroll-employee/{id}', 'PayrollController@payrollEmployee');

        Route::get('requests', 'RequestController@index');
             
        //notification
        Route::post('post-notification','NotificationController@postNotification');
        Route::get('get-notification','NotificationController@getNotification');
        Route::get('show-comment','NotificationController@getComment');
        Route::post('post-comment','NotificationController@postComment');
    });

    Route::group(['namespace' => 'Employee', 'prefix' => 'employee', 'middleware' => 'employee'], function() {
        Route::post('payroll-request', 'RequestController@submitPayrollRequest');
        Route::get('wage-story','WageStoryController@getStory');
        Route::get('get-payrol-request','ShowRequestController@getPayrollRequest');
        Route::post('feedback-request', 'FeedbackController@submitFeedbackRequest');
        Route::get('get-notification','NotificationController@getNotification');
        Route::get('shifts', 'ShiftController@getShifts');
        Route::post('leave-request', 'LeaveRequestController@submitLeaveRequest');
        
        Route::put('update-profile','ProfileController@updateProfile');
        Route::get('get-profile','ProfileController@getProfile' );
        Route::get('shifts/{id}', 'ShiftController@detailShift');
        Route::post('leave-shift-request', 'RequestController@submitLeaveShiftRequest');

        Route::get('shift-in-day/{id}','ShiftController@shiftInDay');
        Route::post('change-shift-request','RequestController@submitChangeShiftRequest');
        
        Route::get('show-leave-shift-request','ShowRequestController@getPendingLeaveShiftRequest');
        Route::get('show-change-shift-request','ShowRequestController@getChangeShiftRequest');
        Route::get('show-leave-request','ShowRequestController@getLeaveRequest');
        Route::get('show-payroll-request','ShowRequestController@getPayrollRequest');
        Route::get('show-shifts-in-store', 'ShiftController@shiftsInStore');

        Route::get('show-comment','NotificationController@getComment');
        Route::post('post-comment','NotificationController@postComment');
        Route::get('attendance','AttendanceController@index');
        Route::get('salary', 'AttendanceController@salary');
        Route::get('all-attendance', 'AttendanceController@allAttendance');
        Route::get('all-salary', 'AttendanceController@allSalary');
        
        Route::post('post-message','MessageController@postMessage');
    });
    
});

Route::post('login/admin', 'AuthController@loginAdmin');
Route::post('login/employee', 'AuthController@loginEmployee');